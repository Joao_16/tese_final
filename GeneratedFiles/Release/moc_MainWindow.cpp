/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../MainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[19];
    char stringdata0[318];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 13), // "displayWindow"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 22), // "frameInfoButtonControl"
QT_MOC_LITERAL(4, 49, 9), // "upLoadBox"
QT_MOC_LITERAL(5, 59, 11), // "generateBox"
QT_MOC_LITERAL(6, 71, 28), // "on_frame_type_picker_toggled"
QT_MOC_LITERAL(7, 100, 7), // "checked"
QT_MOC_LITERAL(8, 108, 26), // "on_location_picker_toggled"
QT_MOC_LITERAL(9, 135, 25), // "on_players_picker_toggled"
QT_MOC_LITERAL(10, 161, 24), // "on_radioButton_2_toggled"
QT_MOC_LITERAL(11, 186, 22), // "generateSearchQueryBox"
QT_MOC_LITERAL(12, 209, 19), // "querryButtonHanddle"
QT_MOC_LITERAL(13, 229, 12), // "partSelected"
QT_MOC_LITERAL(14, 242, 18), // "fieldButtonClicked"
QT_MOC_LITERAL(15, 261, 16), // "teamSideHanddler"
QT_MOC_LITERAL(16, 278, 9), // "checkMenu"
QT_MOC_LITERAL(17, 288, 6), // "button"
QT_MOC_LITERAL(18, 295, 22) // "generateHighlightVideo"

    },
    "MainWindow\0displayWindow\0\0"
    "frameInfoButtonControl\0upLoadBox\0"
    "generateBox\0on_frame_type_picker_toggled\0"
    "checked\0on_location_picker_toggled\0"
    "on_players_picker_toggled\0"
    "on_radioButton_2_toggled\0"
    "generateSearchQueryBox\0querryButtonHanddle\0"
    "partSelected\0fieldButtonClicked\0"
    "teamSideHanddler\0checkMenu\0button\0"
    "generateHighlightVideo"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   89,    2, 0x0a /* Public */,
       3,    0,   90,    2, 0x0a /* Public */,
       4,    0,   91,    2, 0x0a /* Public */,
       5,    0,   92,    2, 0x0a /* Public */,
       6,    1,   93,    2, 0x0a /* Public */,
       8,    1,   96,    2, 0x0a /* Public */,
       9,    1,   99,    2, 0x0a /* Public */,
      10,    1,  102,    2, 0x0a /* Public */,
      11,    0,  105,    2, 0x0a /* Public */,
      12,    0,  106,    2, 0x0a /* Public */,
      13,    0,  107,    2, 0x0a /* Public */,
      14,    0,  108,    2, 0x0a /* Public */,
      15,    0,  109,    2, 0x0a /* Public */,
      16,    1,  110,    2, 0x0a /* Public */,
      18,    0,  113,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QObjectStar,   17,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->displayWindow(); break;
        case 1: _t->frameInfoButtonControl(); break;
        case 2: _t->upLoadBox(); break;
        case 3: _t->generateBox(); break;
        case 4: _t->on_frame_type_picker_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_location_picker_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_players_picker_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_radioButton_2_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->generateSearchQueryBox(); break;
        case 9: _t->querryButtonHanddle(); break;
        case 10: _t->partSelected(); break;
        case 11: _t->fieldButtonClicked(); break;
        case 12: _t->teamSideHanddler(); break;
        case 13: _t->checkMenu((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 14: _t->generateHighlightVideo(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

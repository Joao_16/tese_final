#include "MainWindow.h"
#include <cstdlib>
#include <stdlib.h>
#include <ctime>

using namespace std;


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	this->setFixedSize(1260, 713);
	currentIndex = 0;
	ui.stackedWidget->setCurrentIndex(3);
	//ui.generate_xml_button->setCheckable(true);
	teamPath = " ";

	//LOADING DATA FROM XML FILE AND LOAD VIDEO
	pVideo = ProcessVideo(" ");
	data = UIDataConnect();
	data.loadVideoData(" ");

	//SETUP BUTTON CLICKED
	setupMenuButtonClicked();
	setupFrameInfoButtons();
	setupGenerateUploadButtons();

	//SETUP FRAME INFO
	setupImageView(pVideo.getFrameAtIndex(0));
	setupFrameInfoLabels();

	//SETUP LOCATION MAP
	currentPart = 1;
	setupLocationMap();
	setupLocationButtons();
//	ui.first_button->setDown(true);

	//SETUP QUERY PAGE
	setupQueryPickers();
	setupQueryButtonClicked();
	setupPlayerNamesView();

	//SETUP GENERATE VIDEO PAGE
	setupHighlightButtons();

}

/*
	******************
	***MENU BUTTONS***
	******************
*/

void MainWindow::setupMenuButtonClicked()
{
	connect(ui.generate_xml_button, SIGNAL(clicked()), this, SLOT(displayWindow()));
	connect(ui.search_button, SIGNAL(clicked()), this, SLOT(displayWindow()));
	connect(ui.field_map_button, SIGNAL(clicked()), this, SLOT(displayWindow()));
	connect(ui.query_button, SIGNAL(clicked()), this, SLOT(displayWindow()));
	connect(ui.generate_highlight_button, SIGNAL(clicked()), this, SLOT(displayWindow()));
}

void MainWindow::displayWindow() {

	QObject* button = QObject::sender();

	if (button == ui.generate_xml_button) {
		ui.stackedWidget->setCurrentIndex(3); 
	}
	else if (button == ui.search_button) {
		ui.stackedWidget->setCurrentIndex(0);
	}
	else if (button == ui.field_map_button) {
		ui.stackedWidget->setCurrentIndex(1);
	}
	else if (button == ui.query_button) {
		ui.stackedWidget->setCurrentIndex(2);
	}
	else if (button == ui.generate_highlight_button) {
		ui.stackedWidget->setCurrentIndex(4); 
	}
	
	checkMenu(button);
}

void MainWindow::checkMenu(QObject* button) {

	if (button == ui.generate_xml_button) {
		ui.generate_xml_button->setCheckable(true);
		ui.generate_xml_button->setChecked(true);
	}
	else {
		ui.generate_xml_button->setCheckable(false);
		ui.generate_xml_button->setChecked(false);
	}

	if (button == ui.search_button) {
		ui.search_button->setCheckable(true);
		ui.search_button->setChecked(true);
	}
	else {
		ui.search_button->setCheckable(false);
		ui.search_button->setChecked(false);
	}
	if (button == ui.field_map_button) {
		ui.field_map_button->setCheckable(true);
		ui.field_map_button->setChecked(true);
	}
	else {
		ui.field_map_button->setCheckable(false);
		ui.field_map_button->setChecked(false);
	}
	if (button == ui.query_button) {
		ui.query_button->setCheckable(true);
		ui.query_button->setChecked(true);
	}
	else {
		ui.query_button->setCheckable(false);
		ui.query_button->setChecked(false);
	}

	if (button == ui.generate_highlight_button) {
		ui.generate_highlight_button->setCheckable(true);
		ui.generate_highlight_button->setChecked(true);
	}
	else {
		ui.generate_highlight_button->setCheckable(false);
		ui.generate_highlight_button->setChecked(false);
	}
}

/*
		*******************
		**FRAME INFO PAGE**
		*******************
*/

void MainWindow::setupFrameInfoButtons()
{
	connect(ui.next_frame_button, SIGNAL(clicked()), this, SLOT(frameInfoButtonControl()));
	connect(ui.previous_button, SIGNAL(clicked()), this, SLOT(frameInfoButtonControl()));
	connect(ui.random_frame_button, SIGNAL(clicked()), this, SLOT(frameInfoButtonControl()));
	connect(ui.frame_bynumber_buttton, SIGNAL(clicked()), this, SLOT(frameInfoButtonControl()));

	ui.max_frame->setText("Max Frame: " + QString::number(data.getTotalFrames() - 1));
}

void MainWindow::frameInfoButtonControl()
{

	QObject* button = QObject::sender();

	if (button == ui.next_frame_button) {
		if(currentIndex + 25 < data.getTotalFrames()*25)
			currentIndex += 25;
	}
	else if (button == ui.previous_button){
		if (currentIndex  > 0)
			currentIndex -= 25;
	}
	else if (button == ui.random_frame_button) {
		srand(time(0));
		currentIndex = rand() % data.getTotalFrames() * 25;
	}
	else if (button == ui.frame_bynumber_buttton) {
		currentIndex = ui.frame_bynumber_TextField->text().toInt();
		currentIndex = currentIndex * 25;
		ui.frame_bynumber_TextField->clear();
	}

	field->hide();
	setupImageView(data.drawRectFaces(pVideo.getFrameAtIndex(currentIndex),currentIndex));
	setupFrameInfoLabels();
}

void MainWindow::setupImageView(cv::Mat img) {

	cv::cvtColor(img, img, CV_BGR2RGB);
	cv::resize(img, img, cv::Size(720, 455));
	QImage image((unsigned char*)img.data, img.cols, img.rows, QImage::Format_RGB888);
	QGraphicsScene* scene = new QGraphicsScene;
	scene->addPixmap(QPixmap::fromImage(image));
	ui.graphicsView->setScene(scene); 
	ui.graphicsView->setStyleSheet("border:none");
}

void MainWindow::setupFrameInfoLabels()
{
	for(int i = 0; i < labels.size();i++){
		labels.at(i)->clear();
	}
	FrameInfo f = data.getFrame(currentIndex);

	ui.frame_id->setText("Frame ID: " + QString::number(f.getID()));
	ui.current_video_time->setText("Current Video Time: " + QString::number(f.getVideoTime()));

	if (!f.getGameTime().compare("")) {
		ui.current_game_time->setText("Current Game Time: No Time Border Detected");
		ui.current_game_result->setText("Current Game Time: No Result Border Detected");
	}
	else {
		ui.current_game_time->setText(QString::fromStdString("Current Game Time: " + f.getGameTime()));
		ui.current_game_result->setText(QString::fromStdString("Current Game Result: " + f.getGameResult()));
	}

	if (f.getType() == 0) {
		ui.frame_type->setText("Frame Type: Out Of Field");
	}
	else if (f.getType() == 1) {
		ui.frame_type->setText("Frame Type: Close up");
		
		CloseUpFrame c = data.getCloseUpFrame(f.getID());
		vector<string> names = c.getPlayerList();
		if (names.size() == 0) {
			ui.players_in_frame->setText("Players in frame: No players detected.");
		}
		else {
			ui.players_in_frame->setText("Players in frame: ");
			for (int i = 0; i < names.size(); i++) {
				QLabel *label = new QLabel(ui.groupBox);
				label->setText(QString::fromStdString("- " + names.at(i)));
				label->setGeometry(QRect(15, 270 + 40 * i, 271, 31));
				label->show();
				labels.push_back(label);
			}
		}
	}
	else {
		ui.frame_type->setText("Frame Type: Long/Mid Shot");
		LongMidFrame l = data.getLongMidFrame(f.getID());

		ui.players_in_frame->setText("Number Of Players in Frame: ");

		int i;
		for (i = 0; i < l.getTeamNames().size(); i++) {
			QLabel *label = new QLabel(ui.groupBox);
			string teamName = l.getTeamNames().at(i) + ": ";
			label->setText(QString::fromStdString(teamName + to_string(l.getNumberPlayers().at(i))));
			label->setGeometry(QRect(15, 270 + 40 * i, 271, 31));
			label->show();
			labels.push_back(label);
		}


		QLabel *label = new QLabel(ui.groupBox);
		label->setText(QString::fromStdString("Location: " + to_string(l.getFieldZone())));
		label->setGeometry(QRect(15, 270 + 120, 271, 31));
		label->show();
		labels.push_back(label);

		field = new QPushButton(ui.groupBox);
		field->setGeometry(QRect(5, 270 + 150, 320, 200));
		field->show();
		

		string field_img = "data/field_" + to_string(l.getFieldZone()) + ".jpg";
		cv::Mat img = cv::imread(field_img);
		cv::cvtColor(img, img, CV_BGR2RGB);
		cv::resize(img, img, cv::Size(300, 185));
		QImage image((unsigned char*)img.data, img.cols, img.rows, QImage::Format_RGB888);
		QPixmap pixmap(QPixmap::fromImage(image));
		QIcon ButtonIcon(QPixmap::fromImage(image));
		field->setIcon(ButtonIcon);
		field->setIconSize(pixmap.rect().size());
		field->setStyleSheet("border:none");
	}
}


/*
		*******************
		*LOCATION MAP PAGE*
		*******************
*/
void MainWindow::setupLocationMap()
{
	cv::Mat img = cv::imread("data/football.png");
	cv::cvtColor(img, img, CV_BGR2RGB);
	cv::resize(img, img, cv::Size(1000, 575));
	QImage image((unsigned char*)img.data, img.cols, img.rows, QImage::Format_RGB888);
	QGraphicsScene* scene = new QGraphicsScene(0,0,ui.football_field_view->width(),ui.football_field_view->height());
	scene->addPixmap(QPixmap::fromImage(image));
	ui.football_field_view->setSceneRect(QRect(-10, -10, ui.football_field_view->width(), ui.football_field_view->height()));
	ui.football_field_view->setScene(scene);
}

void MainWindow::setupLocationButtons()
{
	connect(ui.first_button, SIGNAL(clicked()), this, SLOT(partSelected()));
	connect(ui.second_button, SIGNAL(clicked()), this, SLOT(partSelected()));

	drawLocationPoints(1);
}

void MainWindow::partSelected()
{
	QObject* button = QObject::sender();

	if (button == ui.first_button) {
		if(currentPart != 1)
			drawLocationPoints(1);
		ui.first_button->setDown(true);
		ui.label->setText(QString::fromStdString(leftTeam));
		ui.label_2->setText(QString::fromStdString(rightTeam));
	}
	else if(button == ui.second_button){
		if (currentPart != 2)
			drawLocationPoints(2);
		ui.second_button->setDown(true);
		ui.label->setText(QString::fromStdString(rightTeam));
		ui.label_2->setText(QString::fromStdString(leftTeam));
	}
}

void MainWindow::drawLocationPoints(int part)
{
	currentPart = part;
	ui.football_field_view->scene()->clear();
	setupLocationMap();
	Location loc = data.getLocationDistribution(part);
	
	for (int i = 0; i <= 5; i++) {
		int totalPoints = loc.getLocation(i);
		for (int j = 0; j < totalPoints; j++) {
			int x = rand() % (180 * (i + 1) - (180 * i +60)) + (180 * i + 60) - 180;
			int y = rand() % (550 - 60 + 1) + 60;
			QPoint pt = QPoint(x, y);
			ui.football_field_view->mapToScene(pt);
			double rad = 3;
			ui.football_field_view->scene()->addEllipse(pt.x() - rad, pt.y() - rad, rad*2.0, rad*2.0,
				QPen(), QBrush(Qt::SolidPattern));
		}
	}

	ui.total_1->setText(QString::number(loc.getLocationPercentage(1)) + "%");
	ui.total_2->setText(QString::number(loc.getLocationPercentage(2)) + "%");
	ui.total_3->setText(QString::number(loc.getLocationPercentage(3)) + "%");
	ui.total_4->setText(QString::number(loc.getLocationPercentage(4)) + "%");
	ui.total_5->setText(QString::number(loc.getLocationPercentage(5)) + "%");
}
	


/*
	**********************
	*GENERATE/UPLOAD PAGE*
	**********************
*/
void MainWindow::setupGenerateUploadButtons()
{
	ui.team1Picker->clear();
	ui.team2Picker->clear();

	connect(ui.video_button, SIGNAL(clicked()), this, SLOT(generateBox()));
	connect(ui.team_button, SIGNAL(clicked()), this, SLOT(generateBox()));
	connect(ui.faces_button, SIGNAL(clicked()), this, SLOT(generateBox()));
	connect(ui.score_button, SIGNAL(clicked()), this, SLOT(generateBox()));
	connect(ui.generate_button, SIGNAL(clicked()), this, SLOT(generateBox()));
	
	connect(ui.xml_button, SIGNAL(clicked()), this, SLOT(upLoadBox()));
	connect(ui.xml_button_2, SIGNAL(clicked()), this, SLOT(upLoadBox()));
	connect(ui.upload_button, SIGNAL(clicked()), this, SLOT(upLoadBox()));

	ui.team1Picker->addItem(QString::fromStdString(data.teamNames(teamPath).at(1)));
	ui.team1Picker->addItem(QString::fromStdString(data.teamNames(teamPath).at(0)));
	ui.team2Picker->addItem(QString::fromStdString(data.teamNames(teamPath).at(0)));
	ui.team2Picker->addItem(QString::fromStdString(data.teamNames(teamPath).at(1)));

	rightTeam = ui.team2Picker->currentText().toStdString();
	leftTeam = ui.team1Picker->currentText().toStdString();

	connect(ui.team1Picker, SIGNAL(activated(int)), this, SLOT(teamSideHanddler()));
	connect(ui.team2Picker, SIGNAL(activated(int)), this, SLOT(teamSideHanddler()));
	
}

void MainWindow::teamSideHanddler()
{
	QObject* picker = QObject::sender();

	if (picker == ui.team1Picker)
		if (ui.team1Picker->currentText().toStdString().compare(data.teamNames(teamPath).at(0)) == 0)
			ui.team2Picker->setCurrentIndex(1);
		else 
			ui.team2Picker->setCurrentIndex(0);
	else if (picker == ui.team2Picker)
		if (ui.team2Picker->currentText().toStdString().compare(data.teamNames(teamPath).at(0)) == 0) 
			ui.team1Picker->setCurrentIndex(0);
		else 
			ui.team1Picker->setCurrentIndex(1);

	rightTeam = ui.team2Picker->currentText().toStdString();
	leftTeam = ui.team1Picker->currentText().toStdString();

	ui.label->setText(QString::fromStdString(leftTeam));
	ui.label_2->setText(QString::fromStdString(rightTeam));
}

void MainWindow::generateBox()
{
	QObject* button = QObject::sender();

	if (button == ui.video_button) {
		QString filename = QFileDialog::getOpenFileName(this, tr("Select a video to process..."), "", tr("Video Files (*.mp4);;All Files (*)"));
		ui.video_path->setText(filename);
		pVideo = ProcessVideo(filename.toStdString());
	}
	else if (button == ui.team_button) {
		QString filename = QFileDialog::getExistingDirectory(this, tr("Select a directory with teams templates..."));
		ui.team_path->setText(filename);
		setPlayerTeamTemplatePath(filename.toStdString());
		teamPath = filename.toStdString();
		setupGenerateUploadButtons();
	}
	else if (button == ui.faces_button) {
		QString filename = QFileDialog::getExistingDirectory(this, tr("Select a directory with player faces..."));
		ui.faces_path->setText(filename);
		setPlayerFacesPath(filename.toStdString());
	}
	else if (button == ui.score_button) {

		QMessageBox scoreborder;
		scoreborder.setStandardButtons(QMessageBox::Ok);
		scoreborder.setDefaultButton(QMessageBox::Ok);
		cv::Mat img = pVideo.getFrameAtIndex(2555);
		cv::cvtColor(img, img, CV_BGR2RGB);
		QImage image((unsigned char*)img.data, img.cols, img.rows, QImage::Format_RGB888);
	
		int x = ui.x->text().toInt();
		int y = ui.y->text().toInt();
		int h = ui.height->text().toInt();
		int w = ui.width->text().toInt();

		QPainter painter(&image);
		painter.setPen(QPen(Qt::white,3));
		painter.drawRect(QRect(x, y, w, h));
		scoreborder.setIconPixmap(QPixmap::fromImage(image));
		scoreborder.show();
		scoreborder.exec();
	}
	else if (button == ui.generate_button) {
		/*if (!ui.video_path->text().compare("") || !ui.faces_path->text().compare("") || !ui.team_path->text().compare("") || !ui.x->text().compare("")) {
			QMessageBox alertBox;
			alertBox.setText("Please Fill all parameters.");
			alertBox.show();
			alertBox.exec();
		}
		else {*/
			QMessageBox startProcess;
			startProcess.setText("Processing video. Please wait...");
			startProcess.show();
			startProcess.exec();
			
			ProcessVideo *ra = new ProcessVideo(" ");
			ra->processAllFrames();

			startProcess.close();
			startProcess.setText("Processing finished. Check your output folder.");
			startProcess.show();
			startProcess.exec();
		//}
		
	}
}

void MainWindow::upLoadBox()
{
	QObject* button = QObject::sender();
	if (button == ui.xml_button) {
		QString filename = QFileDialog::getOpenFileName(this, tr("Select XML file to process..."), "", tr("XML Files (*.xml);"));
		ui.xml_path->setText(filename);
		
	}
	else if (button == ui.xml_button_2) {
		QString filename = QFileDialog::getOpenFileName(this, tr("Select video..."), "", tr("Video Files (*.mp4);;All Files (*)"));
		ui.upload_video_path->setText(filename);
		pVideo = ProcessVideo(filename.toStdString());
	}

	else if (button == ui.upload_button) {
		if (!ui.xml_path->text().compare("")) {
			QMessageBox alertBox;
			alertBox.setText("Please select a XML file.");
			alertBox.show();
			alertBox.exec();
		}
		else {
			data.loadVideoData(ui.xml_path->text().toStdString());
			QMessageBox alertBox;
			alertBox.setText("XML uploaded.");
			alertBox.show();
			alertBox.exec();

			ui.max_frame->setText("Max Frame: " + QString::number(data.getTotalFrames() - 1));
		}
		
	}
}

/*
	**********************
	******QUERY PAGE******
	**********************
*/

void MainWindow::setupQueryPickers() {
	ui.stackedWidget_2->setCurrentIndex(0);
	ui.frame_type_picker->setChecked(true);

	ui.selec_frame->addItem("Long Mid Shot");
	ui.selec_frame->addItem("Close up Shot");
	ui.selec_frame->addItem("Out of Field Shot");

	vector<string> teamN = data.teamNames(" ");
	for (string name : teamN) {
		ui.team_picker->addItem(QString::fromStdString(name));
	}
}

void MainWindow::on_frame_type_picker_toggled(bool checked)
{
	ui.stackedWidget_2->setCurrentIndex(0);
	queryType = "Frame_Type";
}

void MainWindow::on_location_picker_toggled(bool checked)
{
	ui.stackedWidget_2->setCurrentIndex(1);
	queryType = "Field_Location";

	cv::Mat img = cv::imread("data/football_model.jpg");
	cv::cvtColor(img, img, CV_BGR2RGB);
	cv::resize(img, img, cv::Size(348, 198));
	QImage image((unsigned char*)img.data, img.cols, img.rows, QImage::Format_RGB888);
	QPixmap pixmap(QPixmap::fromImage(image));
	QIcon ButtonIcon(QPixmap::fromImage(image));
	ui.field_button->setIcon(ButtonIcon);
	ui.field_button->setIconSize(pixmap.rect().size());
}

void MainWindow::on_players_picker_toggled(bool checked)
{
	ui.stackedWidget_2->setCurrentIndex(2);
	queryType = "Player_Faces";
}

void MainWindow::on_radioButton_2_toggled(bool checked)
{
	ui.stackedWidget_2->setCurrentIndex(3);
	queryType = "Team_Advantage";
}

void MainWindow::setupQueryButtonClicked() {
	connect(ui.search_frame_button2, SIGNAL(clicked()), this, SLOT(generateSearchQueryBox()));
	connect(ui.search_team_button, SIGNAL(clicked()), this, SLOT(generateSearchQueryBox()));
	connect(ui.search_player_name_button, SIGNAL(clicked()), this, SLOT(generateSearchQueryBox()));
	connect(ui.search_location_button, SIGNAL(clicked()), this, SLOT(generateSearchQueryBox()));

	connect(ui.nextframe_query_button, SIGNAL(clicked()), this, SLOT(querryButtonHanddle()));
	connect(ui.previous_query, SIGNAL(clicked()), this, SLOT(querryButtonHanddle()));
	connect(ui.remove_button, SIGNAL(clicked()), this, SLOT(querryButtonHanddle()));
	connect(ui.extract_button, SIGNAL(clicked()), this, SLOT(querryButtonHanddle()));

	connect(ui.field_button, SIGNAL(clicked()), this, SLOT(fieldButtonClicked()));
}

void MainWindow::generateSearchQueryBox() {

	currentQuerryIndex = 0;
	QObject* button = QObject::sender();

	if (button == ui.search_frame_button2) {
		string type = ui.selec_frame->currentText().toStdString();
		queryResult = data.getFrameType(type); cout << queryResult.size();
	}
	else if (button == ui.search_location_button) {
		queryResult = data.getLocation(selected_loc);
	}
	else if (button == ui.search_team_button) {
		string team = ui.team_picker->currentText().toStdString();
		queryResult = data.getTeamAdvantage(team);
	}
	else if (button == ui.search_player_name_button) {
		vector<string> pNames;
		for (QCheckBox *check : checkBoxes) {
			if (check->isChecked())
				pNames.push_back(check->text().toStdString());
		}
		queryResult = data.getPlayerMoments(pNames);

	}
	if (queryResult.size() > 0) {
		setupImageQueryView(data.drawRectFaces(pVideo.getFrameAtIndex(queryResult.at(currentQuerryIndex).x * 25), queryResult.at(currentQuerryIndex).x * 25));
		setupQuerryLabel();
	}
	else if (queryResult.size() == 0) {
		QMessageBox alert;
		alert.setStandardButtons(QMessageBox::Ok);
		alert.setDefaultButton(QMessageBox::Ok);
		alert.setText("No result found");
		alert.show();
		alert.exec();
	}
}

void MainWindow::setupQuerryLabel(){

	int seconds, minutes, hours;
	seconds = queryResult.at(currentQuerryIndex).y;
	minutes = seconds / 60;
	hours = minutes / 60;
	string time ="- "+ to_string(hours) + ":" + to_string(minutes) + ":" + to_string(seconds);
	ui.time_label->setText(QString::fromStdString(time));
}

void MainWindow::querryButtonHanddle()
{
	QObject* button = QObject::sender();

	if (button == ui.nextframe_query_button) {
		currentQuerryIndex++;
		if (currentQuerryIndex < queryResult.size() && queryResult.size() > 0) {
			setupImageQueryView(pVideo.getFrameAtIndex(queryResult.at(currentQuerryIndex).x * 25));
			setupQuerryLabel();
		}
		if (currentQuerryIndex >= queryResult.size()) {
			QMessageBox alert;
			alert.setStandardButtons(QMessageBox::Ok);
			alert.setDefaultButton(QMessageBox::Ok);
			alert.setText("Last Frame to show.");
			alert.show();
			alert.exec();
		}
	}
	if (button == ui.previous_query) {
		
		if (currentQuerryIndex > 0) {
			currentQuerryIndex--;
			setupImageQueryView(pVideo.getFrameAtIndex(queryResult.at(currentQuerryIndex).x * 25));
			setupQuerryLabel();
		}
	}
	else if (button == ui.remove_button) {
		if (queryResult.size() > 0) {
			queryResult.erase(queryResult.begin() + currentQuerryIndex);
			if (currentQuerryIndex > 0) {
				currentQuerryIndex--;
				setupImageQueryView(pVideo.getFrameAtIndex(queryResult.at(currentQuerryIndex).x * 25));
				setupQuerryLabel();
			}
		}
	}
	else if (button == ui.extract_button) {
		if (queryResult.size() > 0) {
			XMLParser xml = XMLParser();
			xml.saveQueryToXML(queryType, queryResult);
			QMessageBox alert;
			alert.setStandardButtons(QMessageBox::Ok);
			alert.setDefaultButton(QMessageBox::Ok);
			alert.setText("XML Generated go to Output Folder.");
			alert.show();
			alert.exec();
		}
		else {
				QMessageBox alert;
				alert.setStandardButtons(QMessageBox::Ok);
				alert.setDefaultButton(QMessageBox::Ok);
				alert.setText("No Result Found.");
				alert.show();
				alert.exec();
		}
	}
}

void MainWindow::setupImageQueryView(cv::Mat img) {

	cv::cvtColor(img, img, CV_BGR2RGB);
	cv::resize(img, img, cv::Size(648, 398));
	QImage image((unsigned char*)img.data, img.cols, img.rows, QImage::Format_RGB888);
	QGraphicsScene* scene = new QGraphicsScene;
	scene->addPixmap(QPixmap::fromImage(image));
	ui.frame_result_view->setScene(scene);
}

void MainWindow::setupPlayerNamesView()
{
	vector<string> pNames = data.playerNames();
	QWidget *checkList = new QWidget();
	QVBoxLayout *layout = new QVBoxLayout();
	checkList->setLayout(layout);
	for (int i = 0; i < pNames.size(); i++) {
		QCheckBox *checkbox = new QCheckBox(QString::fromStdString(pNames.at(i)));
		checkbox->setCheckState(Qt::Unchecked);
		checkbox->setGeometry(QRect(15, 20 * i, 271, 31));
		QFont f("MS Shell Dlq 2", 12);
		checkbox->setFont(f);
		layout->addWidget(checkbox);
		checkBoxes.push_back(checkbox);
	}
	ui.scrollArea->setWidget(checkList);
}



void MainWindow::fieldButtonClicked()
{
	int xpos = ui.field_button->cursor().pos().x();
	cv::Mat img;
	if (xpos < 932) {
		img = cv::imread("data/field_1.jpg");
		selected_loc = 1;
	}
	else if (xpos >= 932 && xpos < 1005) {
		img = cv::imread("data/field_2.jpg");
		selected_loc = 2;
	}
	else if (xpos >= 1005 && xpos < 1082) {
		img = cv::imread("data/field_3.jpg");
		selected_loc = 3;
	}
	else if (xpos >= 1082 && xpos < 1154) {
		img = cv::imread("data/field_4.jpg");
		selected_loc = 4;
	}
	else if (xpos >= 1154) {
		img = cv::imread("data/field_5.jpg");
		selected_loc = 5;
	}
	cv::cvtColor(img, img, CV_BGR2RGB);
	cv::resize(img, img, cv::Size(348, 198));
	QImage image((unsigned char*)img.data, img.cols, img.rows, QImage::Format_RGB888);
	QPixmap pixmap(QPixmap::fromImage(image));
	QIcon ButtonIcon(QPixmap::fromImage(image));
	ui.field_button->setIcon(ButtonIcon);
	ui.field_button->setIconSize(pixmap.rect().size());
}

/*
	*******************
	*HIGHLIGHT PAGE*
	*******************
*/

void MainWindow::setupHighlightButtons()
{
	connect(ui.video_high_button, SIGNAL(clicked()), this, SLOT(generateHighlightVideo()));
	connect(ui.xml_button_4, SIGNAL(clicked()), this, SLOT(generateHighlightVideo())); 
	connect(ui.generate_video_button, SIGNAL(clicked()), this, SLOT(generateHighlightVideo()));
}

void MainWindow::generateHighlightVideo()
{
	QObject* button = QObject::sender();
	if (button == ui.video_high_button) {
		QString filename = QFileDialog::getOpenFileName(this, tr("Select a video to process..."), "", tr("Video Files (*.mp4);;All Files (*)"));
		ui.video_path_2->setText(filename);
	}
	else if (button == ui.xml_button_4) {
		QString filename = QFileDialog::getOpenFileName(this, tr("Select XML file to process..."), "", tr("XML Files (*.xml);"));
		ui.xml_path_2->setText(filename);
	}
	else if (button == ui.generate_video_button) {
		if (ui.video_path_2->text().compare("") == 1) {
		
			std::vector<int> highlightPoints;
			cv::VideoCapture video = cv::VideoCapture(ui.video_path_2->text().toStdString());
			XMLParser xml = XMLParser();

			int interval = 30;
			double percentage = 0.25;
			if (ui.percentage->text().compare("") != 0 && ui.x_2->text().compare("") != 0) {
				percentage = ui.percentage->text().toDouble() /100;
				interval = ui.x_2->text().toInt();
			}


			if (ui.xml_path_2->text().compare("") == 0) {
				
				ui.status_label->setText("Status: Processing all frame types...");
				ui.status_label->repaint();
				std::vector<int> list = pVideo.getFrameTypes(video);
				xml.saveGenerateVideoInfo(list);
				highlightPoints = data.getAllFramesPercentage(list, interval, percentage, 0);
			}
			else {
				
				highlightPoints = data.getAllFramesPercentage(xml.readGenerateVideoInfo(ui.xml_path_2->text().toStdString()), interval, percentage, 0);
				ui.status_label->setText("Status: Loading XML...");
				ui.status_label->repaint();
			}
			ui.status_label->setText("Status: Generating video please wait...");
			ui.status_label->repaint();
			pVideo.splitVideo(video, highlightPoints);
			ui.status_label->setText("Status: Video generated. Check your directory folder... ");
			ui.status_label->repaint();
		}
		else {
			QMessageBox startProcess;
			startProcess.setText("Select a video path please.");
			startProcess.show();
			startProcess.exec();
		}

	}
}
#pragma once
#include "opencv2\opencv.hpp"

class FrameInfo
{

protected:
	long id;
	int type;
	long videoTime;
	std::string  game_time;
	std::string game_result;

public:
	FrameInfo();
	~FrameInfo();
	virtual long getID();
	virtual void setID(long id);
	/*
		type = 0 -> OutOfField Frame
		type = 1 -> CloseUp Frame
		type = 2 -> LongMidShot Frame
	*/
	virtual int getType();
	virtual void setType(int type);

	virtual long getVideoTime();
	virtual void setVideoTime(long time);

	virtual std::string getGameTime();
	virtual void setGameTime(std::string gameTime);

	virtual std::string getGameResult();
	virtual void setGameResult(std::string gameResult);
};

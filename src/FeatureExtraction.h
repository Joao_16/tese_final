#include "XMLParser.h"


class FeatureExtraction {

private:
	cv::Mat hsvFrame;
	cv::Mat originalImg;
	std::vector<std::string> text;
	XMLParser xmlParser;
public:
	FeatureExtraction();
	FeatureExtraction(cv::Mat img);
	~FeatureExtraction();
	cv::Mat getOriginalImg();
	std::vector<std::string> getText();

	long getVideoTime();

	int getFrameType(cv::Mat img);
	int getFrameType(cv::Mat img, cv::Point colorThreshold);

	int getFieldLocation(cv::Mat img);

	std::vector<int> getNumberOfPlayers(cv::Mat img);
	std::vector<std::string> getTeamNames(cv::Mat img);

	std::vector<std::string> getPlayerNames(cv::Mat img);
	std::vector<cv::Rect> getFacesRect();

	std::string getCurrentGameTime(cv::Mat img);
	std::string getCurrentGameResult();
};
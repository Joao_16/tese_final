#include "PlayersFeatures.h"

using namespace std;
using namespace cv;



const int FACE_WIDTH = 100;
const int FACE_HEIGHT = 100;
const string BASE_DIR = "data\\Faces\\Data\\";
string FACE_DB_FILE_DIR = "data/Faces/playerFaces.yml";
string BASE_DIR_TEAM = "data\\Team\\";
const string FACE_XML_DIR = "data/Utils/Haarcascades/haarcascade_frontalface_alt2.xml";
const string PROFILE_XML_DIR = "data/Utils/Haarcascades/haarcascade_profileface.xml";


Ptr<FaceRecognizer> model;
vector<string> playerNames;
vector<char*> teamNames;
vector<vector<int>> playerPos;
vector<vector<MatND>> listOfPlayerTeams;
bool facesLoaded = false;

CascadeClassifier face_cascade;
CascadeClassifier profile_cascade;

vector<Rect> faces;

bool faceXML = false;


/*
-----------------------------------------------------------------------------------------------------
Detect players Mid/Long Shot
-----------------------------------------------------------------------------------------------------
*/

vector<vector<Point> > findPlayerContours(Mat img) {
	Mat img1 = img.clone(); 
	Mat blur, gray;
	dilation(0, 3, img1, img1);

	GaussianBlur(img1, blur, Size(3, 3), 0, 0);
	cvtColor(blur, gray, CV_BGR2GRAY);

	Canny(gray, gray, 150, 200, 3);
	/// Find contours   
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	/// Draw contours
	Mat drawing = Mat::zeros(gray.size(), CV_8UC3);
	Rect bounding_rect;
	contours = findRectIntersect(contours);
	for (unsigned int i = 0; i < contours.size(); i++) {
		Rect a = boundingRect(contours[i]);

		if (a.area() > 560 && a.area() < 5000 && a.width < 150 && a.height < 150) {
			Scalar color = Scalar(255, 255, 255);
			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
			bounding_rect = boundingRect(contours[i]);
			rectangle(drawing, bounding_rect, Scalar(255, 255, 255), 2, 8, 0);
			rectangle(img, bounding_rect, Scalar(255, 255, 255), 2, 8, 0);
		}
		else {
			contours.erase(contours.begin() + i);
			i--;
		}
	}

	return contours;
}

vector<vector<Point> > findRectIntersect(vector<vector<Point> > contours) {

	for (unsigned int i = 0; i < contours.size() - 1; i++) {
		for (unsigned int k = i + 1; k < contours.size(); k++) {
			if (contours[i] != contours[k]) {
				Rect a = boundingRect(contours[i]);
				Rect b = boundingRect(contours[k]);
				Rect c = a & b;
				if ((double)c.area() >(double)b.area()*0.8 || (double)b.height / (double)b.width < 0.8) {

					contours.erase(contours.begin() + k);
					k--;
				}
			}
		}
	}
	return contours;
}

vector<Mat> extractPlayers(Mat img, vector<vector<Point> > contours) {
	vector<Mat> players;
	
	for (unsigned int i = 0; i < contours.size(); i++) {
		Rect a = boundingRect(contours[i]);
		Mat aux = img(a);
		players.push_back(aux);
	}
	return players;
}

void setPlayersTeam(vector <Mat> players) {

	listOfPlayerTeams.clear();
	playerPos.clear();
	teamNames.clear();
	
	for (unsigned int j = 0; j < players.size(); j++) {

		Mat p = players.at(j);
		MatND playerHistogram = getHistogramRGB(p);

		double maxValue = 0;
		int maxIndex = 0;

		for (unsigned int i = 0; i < listOfPlayerTeams.size(); i++) {
			double aux = compareHistogram(playerHistogram, listOfPlayerTeams.at(i).at(0));
			if (aux > maxValue) {
				maxValue = aux;
				maxIndex = i;
			}
		}
		if (maxValue > 0.6) {
			listOfPlayerTeams.at(maxIndex).push_back(playerHistogram);
			playerPos.at(maxIndex).push_back(j);
		}
		else {
			vector<MatND> newTeam;
			newTeam.push_back(playerHistogram);
			listOfPlayerTeams.push_back(newTeam);
			vector<int> newTeamPos;
			newTeamPos.push_back(j);
			playerPos.push_back(newTeamPos);
		}
	}
	sort(listOfPlayerTeams.begin(), listOfPlayerTeams.end(), less_vectors);

	teamNames = attributeTeam(listOfPlayerTeams);
}

bool less_vectors(const vector<Mat> a, const vector<Mat> b) {
	return a.size() > b.size();
}

MatND getHistogramRGB(Mat player) {

	//Histogram Equalization
	Mat result = histogramEqualization(player);

	int nbins = 8;

	int histSize[3] = { nbins, nbins, nbins };
	float range[2] = { 0, 256 };
	const float * ranges[3] = { range, range, range };

	int channels[3] = { 0, 1, 2 };

	MatND hist;

	calcHist(&result, 1, channels, Mat(), hist, 3, histSize, ranges, true, false);

	return hist;
}

MatND getHistogramHSV(Mat player) {

	Mat hsv;
	cvtColor(player, hsv, COLOR_BGR2HSV);

	// Quantize the hue to 30 levels
	// and the saturation to 32 levels
	int hbins = 8, sbins = 32;
	int histSize[] = { hbins, sbins };
	// hue varies from 0 to 179, see cvtColor
	float hranges[] = { 0, 180 };
	// saturation varies from 0 (black-gray-white) to
	// 255 (pure spectrum color)
	float sranges[] = { 0, 256 };
	const float* ranges[] = { hranges, sranges };
	MatND hist;
	// we compute the histogram from the 0-th and 1-st channels
	int channels[] = { 0, 1 };

	calcHist(&hsv, 1, channels, Mat(), // do not use mask
		hist, 2, histSize, ranges,
		true,
		false);

	return hist;
}

Mat histogramEqualization(Mat player) {

	Mat ycrcb;
	cvtColor(player, ycrcb, CV_BGR2YCrCb);

	vector<Mat> chs;
	split(ycrcb, chs);

	equalizeHist(chs[0], chs[0]);

	Mat result;
	merge(chs, ycrcb);
	cvtColor(ycrcb, result, CV_YCrCb2BGR);

	ycrcb.release();

	return result;
}

double compareHistogram(MatND hist1, MatND hist2) {

	double result = compareHist(hist1, hist2, CV_COMP_CORREL);
	return result;
}

vector<char*> attributeTeam(vector<vector<MatND>> players) {

	double team1Total = 0;
	double team2Total = 0;

	vector<char*> teamNames;

	vector<String> listOfFiles;
	cv::glob(BASE_DIR_TEAM, listOfFiles, false);

	Mat team1Img = imread(listOfFiles[0], CV_LOAD_IMAGE_COLOR);
	Mat team2Img = imread(listOfFiles[1], CV_LOAD_IMAGE_COLOR);



	Rect limits = Rect(team1Img.cols / 2 - 50, 60, 100, team1Img.rows-60);
	team1Img = Mat(team1Img, limits);

	limits = Rect(team2Img.cols / 2 - 50, 60, 100, team2Img.rows-60);
	team2Img = Mat(team2Img, limits);

	resize(team1Img, team1Img, cv::Size(), 0.5, 0.5);
	resize(team2Img, team2Img, cv::Size(), 0.5, 0.5);

	MatND team1Hist = getHistogramRGB(team1Img);
	MatND team2Hist = getHistogramRGB(team2Img);


	for (int y = 0; y < players.size(); y++) {
		if (y > 2 && players.at(y).size() < 2) {
			teamNames.push_back("Referee/Staff/Missmatch");
		}
		else {
			for (unsigned int k = 0; k < players.at(y).size(); k++) {
				team1Total += compareHistogram(players.at(y).at(k), team1Hist);
				team2Total += compareHistogram(players.at(y).at(k), team2Hist);
			}
			if (team1Total > team2Total) {
				string aux = processTeamName(listOfFiles[0]);
				char *cstr = new char[aux.length() + 1];
				strcpy(cstr, aux.c_str());
				teamNames.push_back(cstr);
			}
			else {
				string aux = processTeamName(listOfFiles[1]);
				char *cstr = new char[aux.length() + 1];
				strcpy(cstr, aux.c_str());
				teamNames.push_back(cstr);
			}
			
			team1Total = 0;
			team2Total = 0;
		}
		cout << players.at(y).size() << "  " << teamNames.at(teamNames.size() - 1) << endl;
	}

	team1Img.release(); team1Hist.release();
	team2Img.release(); team2Hist.release();
	return teamNames;
}

string processTeamName(string teamName) {

	size_t pos = teamName.find_last_of("\\");
	teamName = teamName.substr(pos + 1);

	pos = teamName.find(".");

	teamName = teamName.substr(0, pos);
	teamName = teamName.substr(0, teamName.size());

	return teamName;
}

std::vector<int> getTotalPlayers()
{
	vector<int> totalPlayers; 
	for (int j = 0; j < teamNames.size(); j++) {
		int playerNumber = listOfPlayerTeams.at(j).size();
		char* team = teamNames.at(j);
		for (int k = j + 1; k < teamNames.size(); k++) {
			char* aux = teamNames.at(k);
			if (strcmp(team, aux) == 0) {
				playerNumber += listOfPlayerTeams.at(k).size();
				teamNames.erase(teamNames.begin() + k);
				listOfPlayerTeams.erase(listOfPlayerTeams.begin() + k);
				k--;
			}
		}
		totalPlayers.push_back(playerNumber); 
	}
	return totalPlayers;
}

std::vector<char*> getTeams()
{
	return teamNames;
}


void setPlayerTeamTemplatePath(std::string path)
{
	BASE_DIR_TEAM = path;
}


/*
-----------------------------------------------------------------------------------------------------
Detect players Close-up Shot
-----------------------------------------------------------------------------------------------------
*/


vector<Mat> detectPlayersFaces(Mat img) {

	vector<Mat> allPlayerFaces;
	faces.clear();
	Mat gray;
	cvtColor(img, gray, COLOR_BGR2GRAY);
	equalizeHist(gray, gray);

	//reduce size to lower time needed
	cv::resize(gray, gray, cv::Size(), 0.75, 0.75);

	if (!faceXML) {
		face_cascade.load(FACE_XML_DIR);

		profile_cascade.load(PROFILE_XML_DIR);
		faceXML = true;
	}

	// Detect faces
	face_cascade.detectMultiScale(gray, faces, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(20, 20));

	/* Detect faces with profile
	vector<Rect> profile;
	profile_cascade.detectMultiScale(gray, profile, 1.1, 3, 0 | CV_HAAR_SCALE_IMAGE, Size(20, 20));*/

	// Draw rect on the detected faces
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		Mat face = Mat(gray, faces[i]);
		double rows = (double)FACE_HEIGHT / (double)face.rows;
		double cols = (double)FACE_WIDTH / (double)face.cols;
		resize(face, face, cv::Size(), cols, rows);

		allPlayerFaces.push_back(face);
		rectangle(gray, faces[i], Scalar(255, 255, 255), 2, 8, 0);
	}
	imshow("Faces", gray);
	//profile = processMerge(faces, profile);

	/*for (unsigned int i = 0; i < profile.size(); i++)
	{
		Mat face = Mat(gray, profile[i]);
		double rows = (double)FACE_HEIGHT / (double)face.rows;
		double cols = (double)FACE_WIDTH / (double)face.cols;
		resize(face, face, cv::Size(), cols, rows);

		allPlayerFaces.push_back(face);
		rectangle(gray, profile[i], Scalar(255, 255, 255), 2, 8, 0);
	}*/
	return allPlayerFaces;
}


vector<Rect> processMerge(vector<Rect> faces, vector<Rect> profile) {

	for (unsigned int i = 0; i < faces.size(); i++) {
		for (unsigned int k = 0; k < profile.size(); k++) {
			Rect a = faces[i];
			Rect b = profile[k];
			Rect c = a & b;
			if (c.area() > a.area()*0.8) {
				profile.erase(profile.begin() + k);
				k--;
			}
		}
	}
	return profile;
}

vector<string> identifyPlayerDetected(std::vector<cv::Mat> playerFaces) {
	vector<string> detectedPlayers;
	if (!facesLoaded) {
		loadFaceDB();
		facesLoaded = true;
	}
	for (unsigned int i = 0; i < playerFaces.size(); i++) {

		int label; double confidence;
		model->predict(playerFaces.at(i), label, confidence);

		string playerN = processFilename(playerNames.at(label-1));
		if (std::find(detectedPlayers.begin(), detectedPlayers.end(), playerN) != detectedPlayers.end()) {}
		else
			detectedPlayers.push_back(playerN);

		playerFaces.at(i).release();
	}
	return detectedPlayers;
}




void loadFaceDB() {
	playerNames.clear();

	//model = createLBPHFaceRecognizer();
	model = createFisherFaceRecognizer();
	//model = createEigenFaceRecognizer();
	//model->set("threshold", 250.0);

	ifstream infield(FACE_DB_FILE_DIR);

	if (!infield.good()) {

		vector<Mat> faces;
		vector<int> labels;

		vector<String> listOfFiles;
		cv::glob(BASE_DIR, listOfFiles, false);
		for (unsigned int j = 0; j < listOfFiles.size(); j++) {
			string filename = listOfFiles[j];

			Mat img = imread(filename, 0);
			double rows = (double)FACE_HEIGHT / (double)img.rows;
			double cols = (double)FACE_WIDTH / (double)img.cols;
			resize(img, img, cv::Size(), cols, rows);
			faces.push_back(img);
			labels.push_back(j);
			playerNames.push_back(filename);
		}

		model->train(faces, labels);
		model->save(FACE_DB_FILE_DIR);
	}
	else {
		loadPlayerNames();
		model->load(FACE_DB_FILE_DIR);

	}

}

void loadPlayerNames() {
	vector<String> listOfFiles;
	cv::glob(BASE_DIR, listOfFiles, false);
	for (unsigned int j = 0; j < listOfFiles.size(); j++) {
		string filename = listOfFiles[j];
		playerNames.push_back(filename);
	}
}


string processFilename(string playername) {

	size_t pos = playername.find_last_of("\\");
	playername = playername.substr(pos + 1);

	pos = playername.find(".");

	playername = playername.substr(0, pos);
	playername = playername.substr(0, playername.size() - 1);

	return playername;
}

void setPlayerFacesPath(std::string path)
{
	FACE_DB_FILE_DIR = path;
}


std::vector<cv::Rect> getPlayerFacesRect(){
	return faces;
}
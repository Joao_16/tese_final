#pragma once

#include "FieldFeatures.h"
#include "FrameFeatures.h"
#include "PlayersFeatures.h"
#include "SVMClassifier.h"
#include "ScoreBorderFeatures.h"
#include "Frame.h"
#include "FrameInfo.h"
#include "CloseUpFrame.h"
#include "LongMidFrame.h"
#include "OutOfFieldFrame.h"

#include <iostream>
#include <cstring>
#include <string>
#include <map>


#include "opencv2\opencv.hpp"


#define OUT_OF_FIELD 0
#define CLOSE_UP 1
#define LONG_MID 2
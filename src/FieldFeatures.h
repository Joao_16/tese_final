#include "OperatorsUtils.h"


struct PointsLocation;
/*
	Detecta os locais na imagem onde existe relvado
*/
cv::Mat detectField(cv::Mat hsvimg);

/*
	Determina a percentagem de campo presente em cada frame
	Realiza-se uma divis�o da imagem HSV e analisa-se a com-
	ponente V
*/
int fieldPercentage(cv::Mat img);
int rankImage(int fieldPercentage, std::vector<double> splitP);
std::vector<cv::Vec4i> detectFieldLines(cv::Mat src);
void detectFieldCircles(cv::Mat src);
void rankLinesLocation(std::vector<cv::Vec4i> lines, int width, int height);
void processPoint(PointsLocation &pointLoc, cv::Point x, int width, int height);

void locateFrame(std::vector<cv::Vec4i> lines);
bool checkCloseLines(std::vector<cv::Vec4i> lines, int pos);


cv::Point mainColorThreshold(cv::Mat img);

cv::MatND getHistogramHSV2(cv::Mat img);

cv::Mat detectFieldWithDynamicThreshold(cv::Mat hsvimg, cv::Point threshold);
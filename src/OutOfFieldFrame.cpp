#include "OutOfFieldFrame.h"

OutOfFieldFrame::OutOfFieldFrame(FrameInfo f)
{
	setID(f.getID());
	setGameResult(f.getGameResult());
	setGameTime(f.getGameTime());
	setType(f.getType());
	setVideoTime(f.getVideoTime());
}

OutOfFieldFrame::~OutOfFieldFrame()
{
}

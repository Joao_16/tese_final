#include "FrameInfo.h"

FrameInfo::FrameInfo()
{
}

FrameInfo::~FrameInfo()
{
}

long FrameInfo::getID()
{
	return id;
}

void FrameInfo::setID(long id)
{
	this->id = id;
}

int FrameInfo::getType()
{
	return type;
}

void FrameInfo::setType(int type)
{
	this->type = type;
}

long FrameInfo::getVideoTime()
{
	return videoTime;
}

void FrameInfo::setVideoTime(long time)
{
	this->videoTime = time;
}

std::string FrameInfo::getGameTime()
{
	return game_time;
}

void FrameInfo::setGameTime(std::string gameTime)
{
	this->game_time = gameTime;
}

std::string FrameInfo::getGameResult()
{
	return game_result;
}

void FrameInfo::setGameResult(std::string gameResult)
{
	this->game_result = gameResult;
}

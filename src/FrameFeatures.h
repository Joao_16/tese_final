#include "opencv2\opencv.hpp"
#include "OperatorsUtils.h"



/*
	Desenho da divisao 3:5:3
*/
void drawLines(cv::Mat img);

/*
	Divisao do frame em 3:5:3, posicoes
	de cada bloco no vector retornado
	---------------------------------
	|	0	|		1		|	2	|
	---------------------------------
	|	3	|		4		|	5	|
	---------------------------------
	|	6	|		7		|	8	|
	---------------------------------
*/
std::vector<cv::Mat> splitFrame(cv::Mat img);


/*
Retira o ruido da imagem para facilitar a detecao de linhas
*/
void intersectWithMask(cv::Mat src, cv::Mat mask);


void addLayerOfField(std::vector<double> indexOfField, std::vector<cv::Mat> splitFrame);

void joinIntersectMat(std::vector<cv::Mat> splitF, cv::Mat img);
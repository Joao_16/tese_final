#pragma once
#include "opencv2\opencv.hpp"
#include "OperatorsUtils.h"
#include <tesseract\baseapi.h>
#include <leptonica\allheaders.h>
#include <string> 



std::vector<cv::Rect>  detectTextBoxes(cv::Mat img);
char* recognizeText(cv::Mat img);
std::vector<std::string> processScoreBorder(cv::Mat img);
bool compare_rect(const cv::Rect &a, const cv::Rect &b);
char* getTime(std::vector<std::string> text);
std::string getResult(std::vector<std::string> text);

#include "LongMidFrame.h"

LongMidFrame::LongMidFrame(FrameInfo f)
{
	setID(f.getID());
	setGameResult(f.getGameResult());
	setGameTime(f.getGameTime());
	setType(f.getType());
	setVideoTime(f.getVideoTime());
}

LongMidFrame::LongMidFrame()
{
}

LongMidFrame::~LongMidFrame()
{
}

std::vector<int> LongMidFrame::getNumberPlayers()
{
	return numberPlayers;
}

void LongMidFrame::setNumberPlayers(std::vector<int> numberPlayers)
{
	this->numberPlayers = numberPlayers;
}

std::vector<std::string> LongMidFrame::getTeamNames()
{
	return teamNames;
}

void LongMidFrame::setTeamNames(std::vector<std::string> teamNames)
{
	this->teamNames = teamNames;
}

int LongMidFrame::getFieldZone()
{
	return fieldZone;
}

void LongMidFrame::setFieldZone(int fieldZone)
{
	this->fieldZone = fieldZone;
}

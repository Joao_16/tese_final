#pragma once
class Location
{
public:
	Location();
	~Location();

private:
	int loc_1;
	int loc_2;
	int loc_3;
	int loc_4;
	int loc_5;
	int total;

public:
	int getLocation(int loc);
	void setLocation(int loc);
	int getLocationPercentage(int loc);
	int getTotal();
};

#pragma once
#include "FrameInfo.h"


class LongMidFrame : public FrameInfo
{

private:
	std::vector<int> numberPlayers;
	std::vector<std::string> teamNames;
	int fieldZone;

public:
	LongMidFrame(FrameInfo f);
	LongMidFrame();
	~LongMidFrame();
	std::vector<int> getNumberPlayers();
	void setNumberPlayers(std::vector<int> numberPlayers);
	std::vector<std::string> getTeamNames();
	void setTeamNames(std::vector<std::string> teamNames);
	int getFieldZone();
	void setFieldZone(int fieldZone);
};

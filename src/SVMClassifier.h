#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml.hpp>
#include "FieldFeatures.h"
#include "FrameFeatures.h"
#include <fstream> 
#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"


void loadDataCentroid();

void setupBOWDescriptor();

int evaluateImage(cv::Mat img);

void setupSVM(cv::Mat trainData, cv::Mat labels);
#include "UIDataConnect.h"


/*
////////////////////////////////
/   UI CONNECT FUNCTIONS	   /
////////////////////////////////
*/

using namespace std;
using namespace cv;

UIDataConnect::UIDataConnect()
{
}


UIDataConnect::~UIDataConnect()
{
}

void UIDataConnect::loadVideoData(std::string xmlPath)
{
	XMLParser xml = XMLParser();
	if (xmlPath.compare(" ") == 0)
		xmlPath = "data/Output/Game.xml";
	xml.loadFrameInfo(xmlPath); 
	listOfCloseUp = xml.getCloseUpFrames(); cout << listOfCloseUp.size();
	listOfLongMid = xml.getLongMidFrames();
	listOfOutOfField = xml.getOutOfFieldFrames();
	listOfFrames = xml.getListOfFrames();
	getLocationDistribution(1);
}

/*
////////////////////////////////
/   GET MAIN DATA			   /
////////////////////////////////
*/

FrameInfo UIDataConnect::getFrame(int index) {
	
	return listOfFrames.at(index/25);
}

CloseUpFrame UIDataConnect::getCloseUpFrame(int id)
{
	for (int i = 0; i < listOfCloseUp.size(); i++)
		if (listOfCloseUp.at(i).getID() == id)
			return listOfCloseUp.at(i);
}

LongMidFrame UIDataConnect::getLongMidFrame(int id)
{
	for (int i = 0; i < listOfLongMid.size(); i++)
		if (listOfLongMid.at(i).getID() == id)
			return listOfLongMid.at(i);
}

OutOfFieldFrame UIDataConnect::getOutOfFieldFrame(int id)
{
	for (int i = 0; i < listOfOutOfField.size(); i++)
		if (listOfOutOfField.at(i).getID() == id)
			return listOfOutOfField.at(i);
}

cv::Mat UIDataConnect::drawRectFaces(cv::Mat img,int index)
{
	FrameInfo f = getFrame(index);
	if (f.getType() == 1) {
		CloseUpFrame c = getCloseUpFrame(f.getID());
		for (int i = 0; i < c.getPlayerFacesRect().size(); i++) {
			Rect aux = c.getPlayerFacesRect().at(i);
			rectangle(img, aux, Scalar(255, 255, 255), 2, 8, 0);
		}
	}
	return img;
}

int UIDataConnect::getTotalFrames() {
	return listOfFrames.size();
}

Location UIDataConnect::getLocationDistribution(int part)
{
	Location loc = Location();
	if (part == 1)
		for (int i = 0; i < listOfLongMid.size() / 2; i++) {
			loc.setLocation(listOfLongMid.at(i).getFieldZone());
		}
	else
		for (int i = listOfLongMid.size() / 2; i < listOfLongMid.size(); i++) {
			
			loc.setLocation(listOfLongMid.at(i).getFieldZone());
		}
	return loc;
}


/*
////////////////////////////////
/     QUERRY RESULTS		   /
////////////////////////////////
*/

std::vector<cv::Point> UIDataConnect::getFrameType(std::string type)
{
	vector<Point> result;

	int frameType = 0;
	if (!type.compare("Long Mid Shot"))
		frameType = 2;
	else if(!type.compare("Close up Shot"))
		frameType = 1;

	for (int i = 0; i < listOfFrames.size(); i++) {
		FrameInfo f = listOfFrames.at(i);	
		if (f.getType() == frameType) {
			Point aux = Point(f.getID(), f.getVideoTime());
			result.push_back(aux);
		}
	}
	return result;
}

std::vector<cv::Point> UIDataConnect::getLocation(int loc)
{
	vector<Point> result;
	for (int i = 0; i < listOfLongMid.size(); i++) {
		LongMidFrame f = listOfLongMid.at(i);
		if (f.getFieldZone() ==  loc) {
			Point aux = Point(f.getID(), f.getVideoTime());
			result.push_back(aux);
		}
	}
	return result;
}

std::vector<cv::Point> UIDataConnect::getTeamAdvantage(std::string team)
{
	vector<Point> result;
	for (int i = 0; i < listOfLongMid.size(); i++) {
		LongMidFrame f = listOfLongMid.at(i);
		int totalP = 0;
		int aux = 0;
		for(int j = 0; j  < f.getTeamNames().size(); j++){
			if (!f.getTeamNames().at(j).compare(team))
				totalP = f.getNumberPlayers().at(j);
			else if (f.getTeamNames().at(j).compare(team) && f.getTeamNames().at(j).compare("Referee/Staff/Missmatch"))
				aux = f.getNumberPlayers().at(j);
			if (totalP > aux) {
				Point a = Point(f.getID(), f.getVideoTime());
				result.push_back(a);
			}
		}
	}
	return result;
}

std::vector<cv::Point> UIDataConnect::getPlayerMoments(std::vector<std::string> pNames)
{
	vector<Point> result;
	for (int i = 0; i < listOfCloseUp.size(); i++) {
		CloseUpFrame f = listOfCloseUp.at(i);
		vector<string> playersInFrame = f.getPlayerList();
		for (string pname : pNames) {
			if (find(playersInFrame.begin(), playersInFrame.end(), pname) != playersInFrame.end()) {
				Point aux = Point(f.getID(), f.getVideoTime());
				result.push_back(aux);
				break;
			}
		}
	}
	return result;
}


vector<string>  UIDataConnect::teamNames(std::string path) {
	vector<String> listOfFiles;
	if(path.compare(" ") == 0)
		cv::glob("data\\Team\\", listOfFiles, false);
	else
		cv::glob(path, listOfFiles, false);

	for (int i = 0; i < listOfFiles.size(); i++) {

		string teamName = listOfFiles.at(i);
		size_t pos = teamName.find_last_of("\\");
		teamName = teamName.substr(pos + 1);

		pos = teamName.find(".");

		teamName = teamName.substr(0, pos);
		teamName = teamName.substr(0, teamName.size());
		listOfFiles.at(i) = teamName;
	}
	return listOfFiles;
}

std::vector<std::string>  UIDataConnect::playerNames() {
	vector<String> playerNames;

	vector<String> listOfFiles;
	cv::glob("data\\Faces\\Data\\", listOfFiles, false);

	for (unsigned int j = 0; j < listOfFiles.size(); j++) {
		string playername = listOfFiles[j];
		size_t pos = playername.find_last_of("\\");
		playername = playername.substr(pos + 1);

		pos = playername.find(".");

		playername = playername.substr(0, pos);
		playername = playername.substr(0, playername.size() - 1);

		if (std::find(playerNames.begin(), playerNames.end(), playername) != playerNames.end()){}
		else 
			playerNames.push_back(playername);
		
	}
	return playerNames;
}


///////////////////////////////////////////////
/////////GENERATE VIDEO HIGHLIGHT CODE/////////
///////////////////////////////////////////////

double UIDataConnect::getFramePercentage(std::vector<int> data, int start, int lenght)
{
	int total_2_Frames = 0;
	for (int i = start; i < start + lenght; i++) 
		if (data[i] == 2)
			total_2_Frames++;

	return (double)total_2_Frames / (double)lenght;
}

std::vector<int> UIDataConnect::getAllFramesPercentage(std::vector<int> data, int lenght, double max_threshold, double min_threshold)
{
	int lastMomentDetectedIndex = 0;
	vector<int> result;
	for (int i = 0; i < data.size() - lenght; i++) {
		double percentage = getFramePercentage(data, i, lenght); 
		if (percentage <= max_threshold) {
			if (result.size() == 0)
				result.push_back(i);

			else if (result.size() > 0 && lastMomentDetectedIndex != i - 1)
				result.push_back(i);

			lastMomentDetectedIndex = i;
		}
	}
	
	for (int i = 0; i < result.size(); i++)
			cout << result[i] << endl;


	return result;
}

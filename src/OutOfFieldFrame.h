#pragma once
#include "FrameInfo.h"

class OutOfFieldFrame : public FrameInfo
{
public:
	OutOfFieldFrame(FrameInfo f);
	~OutOfFieldFrame();
};

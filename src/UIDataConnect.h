#include "ProcessVideo.h"
#include "Location.h"
#include <QtWidgets/QMainWindow>

class UIDataConnect {


private:
	std::vector<FrameInfo> listOfFrames;
	std::vector<LongMidFrame> listOfLongMid;
	std::vector<CloseUpFrame> listOfCloseUp;
	std::vector<OutOfFieldFrame> listOfOutOfField;

	int currentID;


public:
	UIDataConnect();

	~UIDataConnect();
	void UIDataConnect::loadVideoData(std::string xmlPath);
	FrameInfo UIDataConnect::getFrame(int index);
	CloseUpFrame UIDataConnect::getCloseUpFrame(int id);
	LongMidFrame UIDataConnect::getLongMidFrame(int id);
	OutOfFieldFrame UIDataConnect::getOutOfFieldFrame(int id);

	cv::Mat UIDataConnect::drawRectFaces(cv::Mat img, int index);
	
	int UIDataConnect::getTotalFrames();
	Location UIDataConnect::getLocationDistribution(int part);

	std::vector<std::string>  UIDataConnect::teamNames(std::string path);
	std::vector<std::string>  UIDataConnect::playerNames();

	//QUERRIES
	std::vector<cv::Point> UIDataConnect::getFrameType(std::string type);
	std::vector<cv::Point> UIDataConnect::getLocation(int loc);
	std::vector<cv::Point> UIDataConnect::getTeamAdvantage(std::string team);
	std::vector<cv::Point> UIDataConnect::getPlayerMoments(std::vector<std::string> pNames);


	//HIGHLIGHT VIDEO
	double getFramePercentage(std::vector<int> data, int start, int lenght);

	std::vector<int> getAllFramesPercentage(std::vector<int> data, int lenght, double max_threshold, double min_threshold);
};
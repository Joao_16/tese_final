#include "Location.h"

Location::Location()
{
	loc_1 = 0;
	loc_2 = 0;
	loc_3 = 0;
	loc_4 = 0;
	loc_5 = 0;
	total = 0;
}

Location::~Location()
{
}

int Location::getLocation(int loc)
{
	switch (loc)
	{
	case 1:
		return loc_1;
		break;
	case 2:
		return loc_2 ;
		break;
	case 3:
		return loc_3 ;
		break;
	case 4:
		return loc_4;
		break;
	case 5:
		return loc_5;
		break;
	default:
		break;
	}
	return 0;
}

int Location::getLocationPercentage(int loc)
{
	switch (loc)
	{
	case 1:
		return ((double)loc_1 / (double)getTotal()) *100.0;
		break;
	case 2:
		return ((double)loc_2 / (double)getTotal()) * 100.0;
		break;
	case 3:
		return ((double)loc_3 / (double)getTotal()) * 100.0;
		break;
	case 4:
		return ((double)loc_4 / (double)getTotal()) * 100.0;
		break;
	case 5:
		return ((double)loc_5 / (double)getTotal()) * 100.0;
		break;
	default:
		break;
	}
	return 0;
}

void Location::setLocation(int loc)
{
	switch (loc)
	{
	case 1:
		loc_1++;
		break;
	case 2:
		loc_2++;
		break;
	case 3:
		loc_3++;
		break;
	case 4:
		loc_4++;
		break;
	case 5:
		loc_5++;
		break;
	default:
		break;
	}
}

int Location::getTotal()
{
	return loc_1 + loc_2 + loc_3 + loc_4 + loc_5;
}



#include "FrameFeatures.h"


using namespace std;
using namespace cv;


void drawLines(Mat img) {

	int height, width, minHeight, minWidth;
	height = img.rows;
	width = img.cols;
	minHeight = img.rows / 11;
	minWidth = img.cols / 11;


	line(img, Point(minWidth * 3, 0), Point(minWidth * 3, height), Scalar(0, 255, 0), 3);
	line(img, Point(minWidth * 8, 0), Point(minWidth * 8, height), Scalar(0, 255, 0), 3);

	line(img, Point(0, minHeight * 3), Point(width, minHeight * 3), Scalar(0, 255, 0), 3);
	line(img, Point(0, minHeight * 8), Point(width, minHeight * 8), Scalar(0, 255, 0), 3);

}


vector<Mat> splitFrame(Mat img) {

	vector<Mat> frame;
	int minHeight, minWidth;

	minHeight = img.rows / 11;
	minWidth = img.cols / 11;

	frame.push_back(Mat(img, Rect(0, 0, minWidth * 3, minHeight * 3)));
	frame.push_back(Mat(img, Rect(minWidth * 3, 0, minWidth * 5, minHeight * 3)));
	frame.push_back(Mat(img, Rect(minWidth * 8, 0, minWidth * 3, minHeight * 3)));

	frame.push_back(Mat(img, Rect(0, minHeight * 3, minWidth * 3, minHeight * 5))); 
	frame.push_back(Mat(img, Rect(minWidth * 3, minHeight * 3, minWidth * 5, minHeight * 5))); 
	frame.push_back(Mat(img, Rect(minWidth * 8, minHeight * 3, minWidth * 3, minHeight * 5)));

	frame.push_back(Mat(img, Rect(0, minHeight * 8, minWidth * 3, minHeight * 3)));
	frame.push_back(Mat(img, Rect(minWidth * 3, minHeight * 8, minWidth * 5, minHeight * 3)));
	frame.push_back(Mat(img, Rect(minWidth * 8, minHeight * 8, minWidth * 3, minHeight * 3)));

	return frame;
}



void intersectWithMask(Mat src, Mat mask) {

	for (int i = 0; i < mask.rows; ++i)
	{
		Vec3b* pixel = mask.ptr<Vec3b>(i);// point to first pixel in row
		Vec3b* pixelSrc = src.ptr<Vec3b>(i);
		for (int j = 0; j < mask.cols; ++j)
		{
			if (pixel[j][0] == 0 && pixel[j][1] == 0 && pixel[j][2] == 0) {
				pixelSrc[j][0] = pixel[j][0];
				pixelSrc[j][1] = pixel[j][1];
				pixelSrc[j][2] = pixel[j][2];
			}

		}
	}
	dilation(0, 1, src, src);
	//imshow("Original", src);
}


void addLayerOfField(vector<double> indexOfField, vector<Mat> splitF) {

	for (unsigned int i = 0; i < splitF.size(); i++) {
		if (i < 3) {
			double value = indexOfField.at(i);
			if (value < 60 && indexOfField.at(i + 3)> 80) {
				Mat aux = splitF.at(i);
				Mat newSplitBlock = aux;
				
				for (int k = 0; k < aux.rows; ++k) {
					int startingRow = (int) aux.rows - (aux.rows*(value + 10)) / 100;
					if (k < startingRow) {
						Vec3b* pixel = aux.ptr<Vec3b>(k);
						Vec3b* pixelSrc = newSplitBlock.ptr<Vec3b>(k);
						for (int j = 0; j < aux.cols; ++j) {
							if (pixel[j][0] == 0 && pixel[j][1] == 0 && pixel[j][2] == 0) {
								pixelSrc[j][0] = pixel[j][0];
								pixelSrc[j][1] = pixel[j][1];
								pixelSrc[j][2] = pixel[j][2];
							}

						}
					}
				}
				splitF.at(i) = newSplitBlock;
			}
		}
	}
}

void joinIntersectMat(vector<Mat> splitF, Mat img) {

	Mat a,b;
	hconcat(splitF.at(0), splitF.at(1), a);
	hconcat(a, splitF.at(2),b);
	

	b.copyTo(img(cv::Rect(0, 0, b.cols, b.rows)));
	imshow("b", img);
}
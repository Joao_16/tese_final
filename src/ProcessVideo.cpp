#include "ProcessVideo.h"
#include <ctime>


using namespace std;
using namespace cv;

ProcessVideo::ProcessVideo()
{
}

ProcessVideo::ProcessVideo(std::string video_Path)
{
	this->currentID = 0;
	this->currentVideoTime = 0;
	if (video_Path.compare(" ") == 0)
		this->videoPath = "D:/Documentos/Tese_Code/Video/MainVideo.mp4";
	else
		this->videoPath = video_Path;
	cout << videoPath;
	cap = VideoCapture(videoPath);
	totalFrames = cap.get(CV_CAP_PROP_FRAME_COUNT);
	colorThresholdExtracted = false;
}

ProcessVideo::~ProcessVideo()
{
}


//GET FRAME WITH CONSOLE INPUT
cv::Mat ProcessVideo::setNextFrame()
{

	if (!cap.isOpened()) 
		cout << "Fail to load video.";

	/* Mat edges; */
	namedWindow("Video", 1); 
	double frnb(cap.get(CV_CAP_PROP_FRAME_COUNT));
	std::cout << "frame count = " << frnb << endl;
	Mat frame;

	for (;;) {
		
		double fIdx;

		cout << "frame index ? ";
		cin >> fIdx;
		if (fIdx < 0 || fIdx >= frnb) break;
		cap.set(CV_CAP_PROP_POS_FRAMES, fIdx);
		bool success = cap.read(frame);
		if (!success) {
			cout << "Cannot read  frame " << endl;
			break;
		}
		break;
	}
	imshow("Frame requested", frame);
	imwrite("F.png", frame);
	return frame;
}

//GET FRAME FOR THE CICLE OF ALL FRAMES
cv::Mat ProcessVideo::getFrameAtIndex(double index)
{

	Mat frame;
	if (index >= 0 && index < totalFrames) {
		
		cap.set(CV_CAP_PROP_POS_FRAMES, index);
		bool success = cap.read(frame); 
		if (success)
			return frame;
	}

	return frame;
}

void ProcessVideo::processAllFrames()
{
	clock_t cl;     //initializing a clock type

	cl = clock();   //starting time of clock

	cout << "TOTAL FRAMES:" << getTotalFrames();
	for (int i = 0; i < getTotalFrames(); i+=25) {
		cout << "FRAME: " << to_string(i) << "\n";
		Mat frame = getFrameAtIndex(i);
		processFrame(frame);
	}
	cl = clock() - cl;  //end point of clock

	_sleep(1000);   //testing to see if it actually stops at the end point

	cout << "\n TIME TO PROCESS: "<<cl / (double)CLOCKS_PER_SEC << endl;

	XMLParser xml = XMLParser();
	xml.setCloseUpFrames(listOfCloseUp);
	xml.setLongMidFrames(listOfLongMid);
	xml.setOutOfFieldFrames(listOfOutOfField);
	//xml.setListOfFrames(listOfFrames);
	xml.saveFrameInfo();
	
}




/*
	////////////////////////////////
	/CREATE DIFFERENT FRAME OBJECTS/
	////////////////////////////////
*/
void ProcessVideo::processFrame(cv::Mat img)
{
	//CODE FOR ADD TO VECTOR
	// EXAMPLE OF CAST FOR ARRAY LongMidFrame* a = (dynamic_cast <LongMidFrame*> (v.at(0)));

	FrameInfo f = createFrameInfo(img);
	imwrite("Original.png", img);
	int type = f.getType(); 

	if (type == LONG_MID) {
		LongMidFrame l = createLongMidFrame(f, img); 
		listOfLongMid.push_back(l);
		LongMidFrame *lPointer =  new LongMidFrame;
		lPointer = &l;
		listOfFrames.emplace_back(lPointer);		
		
	}
	else if (type == CLOSE_UP) {
		CloseUpFrame c = createCloseUpFrame(f, img);
		listOfCloseUp.push_back(c);
		CloseUpFrame *cPointer = &c;
		listOfFrames.emplace_back(cPointer);
	
	}
	else {
		OutOfFieldFrame o = createOutOfFieldFrame(f);
		listOfOutOfField.push_back(o);
		OutOfFieldFrame *oPointer = &o;
		listOfFrames.emplace_back(oPointer);
	}
	img.release();
}



FrameInfo ProcessVideo::createFrameInfo(cv::Mat img)
{
	
	extractor = FeatureExtraction(img.clone());
	
	FrameInfo frame = FrameInfo();

	frame.setID(currentID++);
	frame.setVideoTime(getCurrentVideoSecond(frame.getID()*25));
		
	int type = 0;
	if (!colorThresholdExtracted)
		type = extractor.getFrameType(img.clone());
	else {
		type = extractor.getFrameType(img.clone(), colorThreshold);
	}
	frame.setType(type);
	Mat scoreBorder = getScoreBorder(extractor.getOriginalImg(),0,0,0,0);
	
	std::string time = extractor.getCurrentGameTime(scoreBorder);
	
	if (time.compare("") != 0) {
		frame.setGameTime(time);
		std::string result = extractor.getCurrentGameResult();
		frame.setGameResult(result);
	}
	else {
		frame.setGameTime("");
		frame.setGameResult("");
	}

	return frame;
}

LongMidFrame ProcessVideo::createLongMidFrame(FrameInfo f, cv::Mat img)
{	
	LongMidFrame frame = LongMidFrame(f);

	if (!colorThresholdExtracted) {
		colorThreshold = mainColorThreshold(img);
		colorThresholdExtracted = true;
	}
	int loc = extractor.getFieldLocation(img);
	frame.setFieldZone(loc); 

	vector<int> totalPlayers = extractor.getNumberOfPlayers(img); 
	frame.setNumberPlayers(totalPlayers); 
	
	frame.setTeamNames(extractor.getTeamNames(img));
	
	return frame;
}

CloseUpFrame ProcessVideo::createCloseUpFrame(FrameInfo f, cv::Mat img)
{
	CloseUpFrame frame = CloseUpFrame(f);

	vector<string> playerlist = extractor.getPlayerNames(img);
	frame.setPlayerList(playerlist);
	frame.setPlayerFacesRect(extractor.getFacesRect());

	return frame;
}

OutOfFieldFrame ProcessVideo::createOutOfFieldFrame(FrameInfo f)
{
	OutOfFieldFrame frame = OutOfFieldFrame(f);
	return frame;
}


/*
////////////////////////////////
////////PRINT FRAME OBJECTS/////
////////////////////////////////
*/

void ProcessVideo::printFrameInfo(int index)
{
	FrameInfo *f = listOfFrames.at(index); 
	
	cout << "\n------------------------" << endl
		<< "ID: " << f->getID() << endl
		<< "VIDEO TIME: " << f->getVideoTime() << endl
		<< "GAME TIME: " << f->getGameTime() << endl
		<< "GAME RESULT: " << f->getGameResult() << endl
		<< "FRAME TYPE: " << f->getType() << endl;
	
	if (f->getType() == LONG_MID) {

		LongMidFrame l = listOfLongMid.at(listOfLongMid.size() - 1);

		cout << "LONG MID SHOT" << endl
			<< "FRAME LOCATION: " << l.getFieldZone() << endl;

		vector<int> nPlayers = l.getNumberPlayers();
		vector<string> nTeams = l.getTeamNames();

		for (unsigned int i = 0; i < nPlayers.size(); i++)
			cout << "TEAM: " << nTeams.at(i) << " NUMBER OF PLAYERS: " << nPlayers.at(i) << " \n";

	}
	else if (f->getType() == CLOSE_UP) {
		CloseUpFrame  c = listOfCloseUp.at(listOfCloseUp.size() - 1);
	
		cout << "CLOSE UP \n";
		vector<string> aux = c.getPlayerList();
		
		if(aux.size() == 0)
			cout << "PLAYERS IN FRAME: CANT DETECT ANY PLAYER \n";
		else {
			cout << "PLAYERS IN FRAME: \n";
			for (unsigned int i = 0; i < aux.size(); i++)
				cout << "-" << aux.at(i) << "\n";
		}
	}
	else {
		OutOfFieldFrame o = listOfOutOfField.at(listOfOutOfField.size() - 1);
		cout << "OUT OF FIELD. \n";
	}
	cout << "---------------------\n";
}


/*
	////////////////////////////////
	/    GET SCORE BOX FROM FRAME  /
	////////////////////////////////
*/
cv::Mat ProcessVideo::getScoreBorder(cv::Mat img, int x, int y,int w,int h)
{
	Rect scoreRect;
	if (w == 0 || h == 0) {
		scoreRect = Rect(195, 47, 225, 32);
	}
	else {
		scoreRect = Rect(x, y, w, h);
	}
	
	Mat scoreBorder = Mat(img, scoreRect);

	return scoreBorder;
}

/*
	////////////////////////////////
	/   GET SECOND FOR EACH FRAME  /	
	////////////////////////////////
*/
int  ProcessVideo::getCurrentVideoSecond(int frameID) {

	double fps = cap.get(CV_CAP_PROP_FPS);
	return frameID / fps;
}

int  ProcessVideo::getTotalFrames() {
	return cap.get(CV_CAP_PROP_FRAME_COUNT);
}



/*
////////////////////////////////
/   GENERATE VIDEO
/
////////////////////////////////
*/
void ProcessVideo::generateVideo(std::string videoPath)
{
	VideoCapture video = VideoCapture(videoPath);
	vector<int> list = getFrameTypes(video);
	XMLParser xml = XMLParser();


	xml.saveGenerateVideoInfo(list);
	//xml.readGenerateVideoInfo("data/Output/VideoHighlight_594.xml");
	vector<int> a{ 2500,5000 };
	splitVideo(video, a);
}

std::vector<int> ProcessVideo::getFrameTypes(cv::VideoCapture video)
{
	int tFrames = video.get(CV_CAP_PROP_FRAME_COUNT);

	int fps = video.get(CV_CAP_PROP_FPS);
	fps = fps / 2;

	vector<int> listOfFrameType;

	for (int i = 0; i < tFrames; i += fps) {
		cout << "Frame: " << i << endl;
		Mat frame;
		
		video.set(CV_CAP_PROP_POS_FRAMES, i);
		bool success = video.read(frame);
		if (success){

			FeatureExtraction ext = FeatureExtraction(frame);

			int type = 0;
			if (!colorThresholdExtracted)
				type = ext.getFrameType(frame);
			else {
				type = ext.getFrameType(frame, colorThreshold);
			}

			if (type == 2 && !colorThresholdExtracted) {
				colorThreshold = mainColorThreshold(frame);
				colorThresholdExtracted = true;
			}
			listOfFrameType.push_back(type);
		}
		frame.release();
	}

	return listOfFrameType;
}

void ProcessVideo::splitVideo(VideoCapture video, vector<int> dataIndex)
{
	
	double fps = video.get(CV_CAP_PROP_FPS);
	size_t width = (size_t)video.get(CV_CAP_PROP_FRAME_WIDTH);
	size_t height = (size_t)video.get(CV_CAP_PROP_FRAME_HEIGHT);
	Size imgSz(width, height);
	size_t totalFrames = (size_t)video.get(CV_CAP_PROP_FRAME_COUNT);


	VideoWriter vidwriter;
	int codec = CV_FOURCC('M', 'J', 'P', 'G');
	vidwriter.open("clip.avi", codec, fps, imgSz, true);
	if (!vidwriter.isOpened())
		fprintf(stderr, "Could not open an output video file for write\n");
	
	else {


		for (int i = 0; i < dataIndex.size(); i++) {

			int startIndex = dataIndex.at(i) * (fps/2);
			cout << "ID: " << dataIndex.at(i) << " FRAME: " << startIndex << endl;
			//NECESSARIO RECUAR 5 SEGUNDOS ANTES DO CLOSE UP ACONTECER PARA REPRESENTAR A JOGADA
			int startPos = 4 * fps;
			if (startIndex - startPos <= 0)
				startIndex = 0;
			else
				startIndex -= startPos;

			Mat frame;
			video.set(CV_CAP_PROP_POS_FRAMES, startIndex);

			//PROLONGA-SE O MOMENTO 2 SEGUNDOS APOS O CLOSE UP DETETADO
			int endIndex = startIndex + startPos + fps * 8;

			for (int frameInd = startIndex; frameInd < endIndex; ++frameInd)
			{
				video >> frame;
				if (frame.empty())
					break;
				vidwriter.write(frame);
			}
			
		}
		video.release();
	}


}



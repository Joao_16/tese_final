#include "ScoreBorderFeatures.h"



using namespace std;
using namespace cv;

vector<Rect> detectTextBoxes(cv::Mat img)
{
	vector<Rect> textBoxes;
	Mat rgb = img;
	Mat gray;
	cvtColor(rgb, gray, CV_BGR2GRAY);
	
	// morphological gradient
	Mat grad;												
	Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(2, 2));
	morphologyEx(gray, grad, MORPH_GRADIENT, morphKernel); 
	// binarize
	Mat bw;
	threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);
	// connect horizontally oriented regions
	Mat connected;								
	morphKernel = getStructuringElement(MORPH_RECT, Size(6, 1));
	morphologyEx(bw, connected, MORPH_CLOSE, morphKernel); 
	// find contours
	Mat mask = Mat::zeros(bw.size(), CV_8UC1);
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	for (int idx = 0; idx >= 0; idx = hierarchy[idx][0])
	{
		Rect rect = boundingRect(contours[idx]); 
		Mat maskROI(mask, rect);
		maskROI = Scalar(0, 0, 0);
		// fill the contour
		drawContours(mask, contours, idx, Scalar(255, 255, 255), CV_FILLED);
		// ratio of non-zero pixels in the filled region
		double r = (double)countNonZero(maskROI) / (rect.width*rect.height);
		if (r > 0.45 // assume at least 45% of the area is filled if it contains text 
			&&
			(rect.height > 8 && rect.width > 8)
			)
		{

			Size inflationSize(rect.width * 0.15f, rect.height * 0.15f);
			Point offset((inflationSize.width - 4)  / 2, (inflationSize.height -4) / 2);
			if (rect.x - offset.x > 0 && rect.y - offset.y > 0) {
				rect += inflationSize;
				rect -= offset;
				if (rect.x + rect.width < img.cols && rect.y + rect.height < img.rows) {
					textBoxes.push_back(rect);
				}
			}
			
		}
	}
	rgb.release(); bw.release(); grad.release(); morphKernel.release(); connected.release(); mask.release();
	return  textBoxes;
}

vector<string> processScoreBorder(Mat img) {

	resize(img, img, cv::Size(), 2, 2);


	vector<Rect> textBoxExtract = detectTextBoxes(img);
	sort(textBoxExtract.begin(), textBoxExtract.end(), compare_rect);
	
	vector<string>textExtract;
	
	for (unsigned int i = 0; i < textBoxExtract.size(); i++) {
		Rect aux = textBoxExtract.at(i);
		
		Mat textBox = Mat(img, textBoxExtract.at(i)); 
		string text = recognizeText(textBox);
		if(text.length() > 2)
			textExtract.push_back(text);
		textBox.release();
	}

	return textExtract;
}

char* recognizeText(cv::Mat img)
{
	//resize(img, img, cv::Size(), 0.75, 0.75);
	Mat gray;
	cvtColor(img, gray, CV_BGR2GRAY);
	threshold(gray, gray, 180, 255, THRESH_BINARY);
	//dilation(0, 1, gray, gray);

	tesseract::TessBaseAPI api;
	api.Init("data/Utils/", "eng", tesseract::OEM_DEFAULT);
	api.SetPageSegMode(static_cast<tesseract::PageSegMode>(7));
	api.SetImage((uchar*)gray.data, gray.size().width, gray.size().height, gray.channels(), gray.step1());
	api.Recognize(0);

	gray.release();
	return api.GetUTF8Text();
}


bool compare_rect(const Rect & a, const Rect &b) {
    return a.x < b.x;
}

char* getTime(vector<string > text)
{
	for (unsigned int i = 0; i < text.size(); i++) {
		string t = text.at(i); 
		if (t.find(":") != string::npos) {
			std::string::iterator end_pos = std::remove(t.begin(), t.end(), '\n');
			t.erase(end_pos, t.end());

			int u = t.find(":");
			string time; 
			time = text.at(i);
		
			char *cstr = new char[time.length() + 1];
			strcpy(cstr, time.c_str());
			return cstr;
		}
	}
	return "";
}

std::string getResult(vector<string> text)
{
	string result;

	for (unsigned int i = 0; i < text.size(); i++) {
		string t = text.at(i);
		if (t.find(":") == string::npos){
			text.at(i).erase(std::remove(text.at(i).begin(), text.at(i).end(), '\n'), text.at(i).end());
			result += text.at(i) + " ";
		}
	}

	return result;
}

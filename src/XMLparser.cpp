#include "XMLParser.h"
#include <sstream>

#define EMOTIONS_FILE_PATH "data/Emotions/PTvsFR.xml"
#define OUTPUT_FILE_PATH "data/Output/Teste.xml"
#define OUTPUT_DIRECTORY_PATH "data/Output/Game.xml"


using namespace std;
using namespace rapidxml;

XMLParser::XMLParser()
{
}

XMLParser::~XMLParser()
{
}


/*
//////////////////////////
		EMOTIONS
//////////////////////////
*/

map<int, char*> XMLParser::loadEmotions()
{
	xml_document<> doc;

	xml_node<> * root_node;
	ifstream theFile(EMOTIONS_FILE_PATH);
	std::stringstream buffer;

	buffer << theFile.rdbuf();
		
	theFile.close();
	std::string content(buffer.str());
	content.push_back('\0');
	doc.parse<0>(&content[0]);
		

	// Find our root node
	xml_node<> *pRoot = doc.first_node();
	if (!theFile)
		cout << "Need XML file with Emotions data.";
	else {
		root_node = doc.first_node("emotionData");
		
		for (xml_node<> *data = root_node->first_node("data"); data; data = data->next_sibling()) {

			xml_node<>* child = data->first_node();
			int timestamp = atoi((char*)child->first_node()->value());
			child = child->next_sibling();
			char* emotion = child->first_node()->value();
	
			emotionsMap.insert(make_pair(timestamp, emotion));
		}
	} 
	return emotionsMap;
}

char * XMLParser::getEmotion(int index)
{
	
	try {
		return emotionsMap.at(index);
	}
	catch (const std::out_of_range& oor) {
		return "";
	}
}



/*
////////////////////////////
		FRAME DATA
////////////////////////////
*/

void XMLParser::loadFrameInfo(std::string filename)
{
	xml_document<> doc_frameInfo;
	cout << filename;
	ifstream theFile(filename);

	std::stringstream buffer;

	buffer << theFile.rdbuf();

	theFile.close();
	std::string content(buffer.str());
	content.push_back('\0');
	doc_frameInfo.parse<0>(&content[0]);

	if (!theFile)
		cout << "Need XML file with Output data.";
	else {
		
		xml_node<>* frameList = doc_frameInfo.first_node("video");
		for (xml_node<>* frame = frameList->first_node(); frame; frame = frame->next_sibling()) {
			FrameInfo f = FrameInfo();
			
			xml_node<>* child = frame->first_node();
			long id = atoi((char*)child->first_node()->value());
			child = child->next_sibling();
			int currentTime = atoi((char*)child->first_node()->value()); 
			child = child->next_sibling();
			char* gameTime = "";
			string gameT = "";
			if (child->first_node() != 0) {
				gameTime = (char*)child->first_node()->value();
				gameT = string(gameTime);
			}
			child = child->next_sibling();
			char* gameResult = "";
			string gameR = "";
			if (child->first_node() != 0) {
				gameResult = (char*)child->first_node()->value();
				gameR = string(gameResult);
			}
			child = child->next_sibling();
			int type = atoi((char*)child->first_node()->value()); 

			f.setID(id); 
			f.setVideoTime(currentTime);
			f.setGameTime(gameT);
			f.setGameResult(gameR);
			f.setType(type); 
			listOfFramesP.push_back(f);

			if (type == 1) {
				CloseUpFrame c = CloseUpFrame(f);
				vector<string> playerNames;
				child = child->next_sibling();
				if (child->first_node() != 0) {
					for (xml_node<>* player = child->first_node(); player; player = player->next_sibling()) {
						xml_node<>* name = player->first_node();
						char* playerN = (char*)name->value();
						string n(playerN);
						playerNames.push_back(n);
					}
				}
				c.setPlayerList(playerNames);
				child = child->next_sibling();
				std::vector<cv::Rect> faces;
				if (child->first_node() != 0) {
					for (xml_node<>* rect = child->first_node(); rect; rect = rect->next_sibling()) {
						int x, y, w, h;

						xml_node<>* rectChild = rect->first_node();
						x = atoi((char*)rectChild->value());
						rectChild = rectChild->next_sibling();
						y = atoi((char*)rectChild->value());
						rectChild = rectChild->next_sibling();
						w = atoi((char*)rectChild->value());
						rectChild = rectChild->next_sibling();
						h = atoi((char*)rectChild->value());
						//std::cout << x << " " << y << " " << w << " " << h << std::endl;
						cv::Rect face = cv::Rect(x, y, w, h);
						faces.push_back(face);
					}
				}
				c.setPlayerFacesRect(faces);


				closeUpFrames.push_back(c);
				CloseUpFrame *cPointer = &c;
				//listOfFramesP.push_back(cPointer);

			}
			else if (type == 2) {
				LongMidFrame l = LongMidFrame(f);
			
				child = child->next_sibling();
				int location = atoi((char*)child->first_node()->value());

				vector<int> teamNPlayers;
				child = child->next_sibling();
				if (child->first_node() != 0) {
					for (xml_node<>* player = child->first_node(); player; player = player->next_sibling()) {
						xml_node<>* name = player->first_node();
						int numberP = atoi((char*)name->value());
						teamNPlayers.push_back(numberP);
					}
				}
				vector<string> teamNames;
				child = child->next_sibling();
				if (child->first_node() != 0) {
					for (xml_node<>* player = child->first_node(); player; player = player->next_sibling()) {
						xml_node<>* name = player->first_node();
						char* teamN = (char*)name->value();
						string n(teamN);
						teamNames.push_back(n);
					}
				}
				l.setFieldZone(location);
				l.setNumberPlayers(teamNPlayers);
				l.setTeamNames(teamNames);
				longMidFrames.push_back(l);
				
				LongMidFrame *lPointer = &l;
				//listOfFramesP.push_back(lPointer); 
			}
			else {
				OutOfFieldFrame o = OutOfFieldFrame(f);
				outOfFieldFrames.push_back(o);
				OutOfFieldFrame *oPointer = &o;
				//listOfFramesP.push_back(oPointer);
			}
		}
	}
}



void XMLParser::saveFrameInfo()
{
	unsigned int currentLong = 0;
	unsigned int currentClose = 0;
	unsigned int currentOut = 0;
	unsigned int maxSize = closeUpFrames.size() + longMidFrames.size() + outOfFieldFrames.size();

	xml_document<> doc;
	vector<LongMidFrame> aux = getLongMidFrames();

	xml_node<>* decl = doc.allocate_node(node_declaration);
	decl->append_attribute(doc.allocate_attribute("version", "1.0"));
	decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
	doc.append_node(decl);

	xml_node<>* root = doc.allocate_node(node_element, "video");
	doc.append_node(root);


	for (int i = 0; i < maxSize; i++) {

		xml_node<>* frame = doc.allocate_node(node_element, "frame");
		root->append_node(frame);
		
		FrameInfo f = FrameInfo();
		if (currentClose < closeUpFrames.size()) {
			if (closeUpFrames.at(currentClose).getID() == i) {
				f = closeUpFrames.at(currentClose++);
			}
		}
		if (currentLong < longMidFrames.size()) {
			if (longMidFrames.at(currentLong).getID() == i) {
				f = longMidFrames.at(currentLong++);
			}
		}
		if (currentOut < outOfFieldFrames.size())
			if (outOfFieldFrames.at(currentOut).getID() == i) {
				f = outOfFieldFrames.at(currentOut++);
			}
		xml_node<>* node = doc.allocate_node(node_element, "id");
		node->value(doc.allocate_string(to_string(f.getID()).c_str()));
		frame->append_node(node);

		node = doc.allocate_node(node_element, "time");
		node->value(doc.allocate_string(to_string(f.getVideoTime()).c_str()));
		frame->append_node(node);

		node = doc.allocate_node(node_element, "gameTime");
		char *cstr = new char[f.getGameTime().length() + 1];
		strcpy(cstr, f.getGameTime().c_str());
		node->value(doc.allocate_string(cstr));
		frame->append_node(node);


		node = doc.allocate_node(node_element, "gameResult");
		char *cstr2 = new char[f.getGameResult().length() + 1];
		strcpy(cstr2, f.getGameResult().c_str());
		node->value(doc.allocate_string(cstr2));
		frame->append_node(node);

		node = doc.allocate_node(node_element, "type");
		node->value(doc.allocate_string(to_string(f.getType()).c_str()));
		frame->append_node(node);

		if (f.getType() == 1) {

			CloseUpFrame c = closeUpFrames.at(currentClose - 1);
			node = doc.allocate_node(node_element, "playerNames");
			for (int j = 0; j < c.getPlayerList().size(); j++) {

				xml_node<>* pNames = doc.allocate_node(node_element, "name");

				string name = c.getPlayerList().at(j);
				char *cstr = new char[name.length() + 1];
				strcpy(cstr, name.c_str());
				pNames->value(doc.allocate_string(cstr));
				node->append_node(pNames);
			}
			frame->append_node(node);

			node = doc.allocate_node(node_element, "playerRect");
			for (int j = 0; j < c.getPlayerFacesRect().size(); j++) {
				xml_node<>* rect = doc.allocate_node(node_element, "rect");

				xml_node<>* rectNode = doc.allocate_node(node_element, "x");
				rectNode->value(doc.allocate_string(to_string(c.getPlayerFacesRect().at(j).x).c_str()));
				rect->append_node(rectNode);

				rectNode = doc.allocate_node(node_element, "y");
				rectNode->value(doc.allocate_string(to_string(c.getPlayerFacesRect().at(j).y).c_str()));
				rect->append_node(rectNode);

				rectNode = doc.allocate_node(node_element, "w");
				rectNode->value(doc.allocate_string(to_string(c.getPlayerFacesRect().at(j).width).c_str()));
				rect->append_node(rectNode);

				rectNode = doc.allocate_node(node_element, "h");
				rectNode->value(doc.allocate_string(to_string(c.getPlayerFacesRect().at(j).height).c_str()));
				rect->append_node(rectNode);

				node->append_node(rect);
			}
			frame->append_node(node);

		}
		else if (f.getType() == 2) {
			LongMidFrame l = longMidFrames.at(currentLong - 1);

			node = doc.allocate_node(node_element, "location");
			node->value(doc.allocate_string(to_string(l.getFieldZone()).c_str()));
			frame->append_node(node);

			node = doc.allocate_node(node_element, "numberOfPlayers");
			for (int j = 0; j < l.getNumberPlayers().size(); j++) {
				xml_node<>* nPlayers = doc.allocate_node(node_element, "team");
				nPlayers->value(doc.allocate_string(to_string(l.getNumberPlayers().at(j)).c_str()));
				node->append_node(nPlayers);
			}
			frame->append_node(node);

			node = doc.allocate_node(node_element, "teamNames");
			for (int k = 0; k < l.getTeamNames().size(); k++) {
				xml_node<>* teamN = doc.allocate_node(node_element, "teamN");
				string name = l.getTeamNames().at(k);
				char *cstr = new char[name.length() + 1];
				strcpy(cstr, name.c_str());
				teamN->value(doc.allocate_string(cstr));
				node->append_node(teamN);
			}
			frame->append_node(node);

		}
		else if (f.getType() == 0) {
			OutOfFieldFrame o = outOfFieldFrames.at(currentOut - 1);
			currentOut++;
		}
	}

	srand(time(0));
	int random = rand() % 1000;
	string filename = "data/Output/Result_"+ to_string(random) + ".xml";
	std::ofstream file_stored(filename);
	file_stored << doc;
	file_stored.close();
	doc.clear();
}
	
		
	

/*
/////////////////////////////
 GET/SET FOR DATA STRUCTURES
/////////////////////////////
*/


std::vector<OutOfFieldFrame> XMLParser::getOutOfFieldFrames()
{
	return outOfFieldFrames;
}

std::vector<CloseUpFrame> XMLParser::getCloseUpFrames()
{
	return closeUpFrames;
}

std::vector<LongMidFrame> XMLParser::getLongMidFrames()
{

	return longMidFrames;
}

std::vector<FrameInfo> XMLParser::getListOfFrames()
{
	return listOfFramesP;
}

void XMLParser::setOutOfFieldFrames(std::vector<OutOfFieldFrame> list)
{
	outOfFieldFrames = list;
}

void XMLParser::setCloseUpFrames(std::vector<CloseUpFrame> list)
{
	closeUpFrames = list;
}

void XMLParser::setLongMidFrames(std::vector<LongMidFrame> list)
{
	longMidFrames = list;
}

void XMLParser::setListOfFrames(std::vector<FrameInfo> list)
{
	listOfFramesP = list;
}

/*
	UI GENERATE XML
*/
void XMLParser::saveQueryToXML(std::string type, std::vector<cv::Point> result)
{
	xml_document<> doc;

	xml_node<>* decl = doc.allocate_node(node_declaration);
	decl->append_attribute(doc.allocate_attribute("version", "1.0"));
	decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
	doc.append_node(decl);

	xml_node<>* root = doc.allocate_node(node_element, "queryResult");
	doc.append_node(root);

	xml_node<>* node = doc.allocate_node(node_element, "type");
	char *cstr2 = new char[type.length() + 1];
	strcpy(cstr2, type.c_str());
	node->value(doc.allocate_string(cstr2));
	root->append_node(node);


	for (int i = 0; i < result.size(); i++) {

		xml_node<>* frame = doc.allocate_node(node_element, "frame");
		root->append_node(frame);

		xml_node<>* node = doc.allocate_node(node_element, "id");
		node->value(doc.allocate_string(to_string(result.at(i).x).c_str()));
		frame->append_node(node);

		node = doc.allocate_node(node_element, "time");
		node->value(doc.allocate_string(to_string(result.at(i).y).c_str()));
		frame->append_node(node);

	}

	srand(time(0));
	int random = rand() % 1000;
	string filename = "data/Output/Query_" + type + "_" + to_string(random) + ".xml";
	std::ofstream file_stored(filename);
	file_stored << doc;
	file_stored.close();
	doc.clear();

}




/*
	GENERATE VIDEO
*/

void XMLParser::saveGenerateVideoInfo(std::vector<int> list)
{

	xml_document<> doc;

	xml_node<>* decl = doc.allocate_node(node_declaration);
	decl->append_attribute(doc.allocate_attribute("version", "1.0"));
	decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
	doc.append_node(decl);

	xml_node<>* root = doc.allocate_node(node_element, "video");
	doc.append_node(root);



	for (int i = 0; i < list.size(); i++) {

		xml_node<>* node = doc.allocate_node(node_element, "type");
		node->value(doc.allocate_string(to_string(list.at(i)).c_str()));
		root->append_node(node);
	}

	srand(time(0));
	int random = rand() % 1000;
	string filename = "data/Output/VideoHighlight_" + to_string(random) + ".xml";
	std::ofstream file_stored(filename);
	file_stored << doc;
	file_stored.close();
	doc.clear();
}

std::vector<int> XMLParser::readGenerateVideoInfo(std::string filepath)
{
	vector<int> list;

	xml_document<> doc_frameInfo;
	cout << filepath;
	ifstream theFile(filepath);
	
	std::stringstream buffer;

	buffer << theFile.rdbuf();

	theFile.close();
	std::string content(buffer.str());
	content.push_back('\0');
	doc_frameInfo.parse<0>(&content[0]);

	if (!theFile)
		cout << "Need XML file with Output data.";
	else {
		
		xml_node<>* frameList = doc_frameInfo.first_node("video");
		for (xml_node<>* frame = frameList->first_node(); frame; frame = frame->next_sibling()) {

			long type = atoi((char*)frame->value());
			list.push_back(type);
		}

	}

	return list;
}

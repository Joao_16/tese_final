#include "SVMClassifier.h"


using namespace std;
using namespace cv;

const int TRAINING_EXAMPLES = 50;
const int NUMBER_OF_ZONES = 5;
const string BASE_DIR = "data\\Classifier\\data\\";
const string BASE_DIR_DIC = "data\\Classifier\\dictionary.yml";
const char* BASE_DIR_XML = "data\\Classifier\\FieldSamplesSVM.xml";
const int IMG_WIDTH = 1280;
const int IMG_HEIGHT = 720;


/*
	BAG OF WORDS IMPLEMENTATION

*/


//--------Using SURF as feature extractor and FlannBased for assigning a new point to the nearest one in the dictionary
Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("FlannBased");
Ptr<DescriptorExtractor> extractor = new SurfDescriptorExtractor();
SurfFeatureDetector detector(500);
//---dictionary size=number of cluster's centroids
int dictionarySize = 1500;
TermCriteria tc(CV_TERMCRIT_ITER, 10, 0.001);
int retries = 1;
int flags = KMEANS_PP_CENTERS;
BOWKMeansTrainer bowTrainer(dictionarySize, tc, retries, flags);
BOWImgDescriptorExtractor bowDE(extractor, matcher);
CvSVM svm;
bool setupDone = false;
int previousFieldZone = 3;




void loadDataCentroid()
{
	int currentRow = 0;
	for (int i = 1; i <= NUMBER_OF_ZONES; i++) {
		string full_dir = BASE_DIR + to_string(i);
		vector<String> listOfFiles;
		cv::glob(full_dir, listOfFiles, false);

		for (unsigned int j = 0; j < listOfFiles.size(); j++) {
			string filename = listOfFiles[j];
			Mat img = imread(filename, CV_LOAD_IMAGE_COLOR);
			Mat hsvimg, resultHSV;

			cvtColor(img, hsvimg, COLOR_BGR2HSV);

			Point hsvThreshold = mainColorThreshold(img);
			resultHSV = detectFieldWithDynamicThreshold(hsvimg, hsvThreshold);
			//resultHSV = detectField(hsvimg);
			intersectWithMask(img, resultHSV);

			vector<KeyPoint> keypoint;
			detector.detect(img, keypoint);
			Mat features;
			extractor->compute(img, keypoint, features);
			bowTrainer.add(features);
		}
		cout << "Finish Directory\n";
	}
}


void setupBOWDescriptor()
{
	if (!setupDone) {
		ifstream infield(BASE_DIR_XML);

		if (!infield.good()) {

			cout << "Vector quantization..." << endl;
			loadDataCentroid();

			vector<Mat> descriptors = bowTrainer.getDescriptors();

			int count = 0;

			for (unsigned int i = 0; i < descriptors.size(); i++) {
				count += descriptors.at(i).rows;
			}
			cout << "Clustering " << count << " features" << endl;

			//choosing cluster's centroids as dictionary's words
			Mat dictionary = bowTrainer.cluster();
			bowDE.setVocabulary(dictionary);

			FileStorage fs(BASE_DIR_DIC, FileStorage::WRITE);
			fs << "vocabulary" << dictionary;
			fs.release();

			cout << "extracting histograms in the form of BOW for each image " << endl;
			Mat labels(0, 1, CV_32FC1);
			Mat trainingData(0, dictionarySize, CV_32FC1);
			int k = 0;
			vector<KeyPoint> keypoint1;
			Mat bowDescriptor1;
			//extracting histogram in the form of bow for each image 

			for (int i = 1; i <= NUMBER_OF_ZONES; i++) {
				string full_dir = BASE_DIR + to_string(i);
				vector<String> listOfFiles;
				cv::glob(full_dir, listOfFiles, false);

				for (unsigned int j = 0; j < listOfFiles.size(); j++) {
					string filename = listOfFiles[j];
					Mat img2 = imread(filename, CV_LOAD_IMAGE_COLOR);
					Mat hsvimg, resultHSV;

					cvtColor(img2, hsvimg, COLOR_BGR2HSV);

					Point hsvThreshold = mainColorThreshold(img2);
					resultHSV = detectFieldWithDynamicThreshold(hsvimg, hsvThreshold);
					intersectWithMask(img2, resultHSV);

					detector.detect(img2, keypoint1);


					bowDE.compute(img2, keypoint1, bowDescriptor1);

					trainingData.push_back(bowDescriptor1);

					labels.push_back((float)i);
				}
				cout << "Finish Directory\n";
			}

			setupSVM(trainingData, labels);

		}
		else {

			Mat dictionary;
			FileStorage fs(BASE_DIR_DIC, FileStorage::READ);
			fs["vocabulary"] >> dictionary;
			fs.release();
			bowDE.setVocabulary(dictionary);

			cout << "Loading SVM from File\n";
			svm.load(BASE_DIR_XML);
			cout << "Setup of SVM Finnished\n";

			dictionary.release();
		}
		setupDone = true;
	}
}


/*
	Ptr<ml::SVM> classifier = ml::SVM::create();
	classifier->setType(ml::SVM::C_SVC);
	classifier->setC(0.1);
	classifier->setKernel(ml::SVM::LINEAR);
	classifier->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, (int)1e7, 1e-6));
*/


void setupSVM(cv::Mat trainData, cv::Mat labels) {

	//Setting up SVM parameters
	CvSVMParams params;
	params.kernel_type = CvSVM::LINEAR;
	//params.kernel_type = CvSVM::RBF;
	params.svm_type = CvSVM::C_SVC;
	params.gamma = 0.50625000000000009;
	params.C = 0.1;
	//params.C = 312.50000000000000;
	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, (int)1e7, 0.000001);
	//params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, 100, 0.000001);

	printf("%s\n", "Training SVM classifier");

	bool res = svm.train(trainData, labels, cv::Mat(), cv::Mat(), params);

	svm.save(BASE_DIR_XML);

	cout << "Training End \n";

}



int evaluateImage(Mat img) {


	Mat hsvimg, resultHSV;
	cvtColor(img, hsvimg, COLOR_BGR2HSV);


	resultHSV = detectField(hsvimg);
	intersectWithMask(img, resultHSV);
	imwrite("jj.png", resultHSV);
	vector<KeyPoint> keypoint2;
	Mat bowDescriptor2;

	detector.detect(img, keypoint2);
	bowDE.compute(img, keypoint2, bowDescriptor2);
	int response = previousFieldZone;
	if (keypoint2.size() > 0) {
		response = (int)svm.predict(bowDescriptor2);
		previousFieldZone = response;
	}
	img.release(); hsvimg.release(); resultHSV.release();
	return response;
}
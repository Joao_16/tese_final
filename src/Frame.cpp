#include "Frame.h"

using namespace cv;
using namespace std;

void Frame::setSplitFrame(vector<Mat> vec) {
	splitFrame = vec;
}

vector<Mat> Frame::getSplitFrame() {
	return splitFrame;
}

Mat Frame::getMaskImg() {
	return maskImg;
}

void Frame::setMask(Mat mask) {
	maskImg = mask;
}

Frame::Frame(std::vector<cv::Mat> splitF, cv::Mat mask) {
	splitFrame = splitF;
	maskImg = mask;

}

Frame::~Frame()
{
}

Mat Frame::getSplitFrameAt(int i) {
	return splitFrame.at(i);
}


void Frame::calculateFieldPercentage() {

	for (unsigned int i = 0; i < splitFrame.size(); i++) {
		double p = fieldPercentage(splitFrame.at(i));
		splitFrameP.push_back(p);
	}

}

vector<double> Frame::getPercentage() {
	calculateFieldPercentage();
	return splitFrameP;
}
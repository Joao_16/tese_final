#pragma once
#include "ProjectHeaders.h"
#include "XML\rapidxml.hpp"
#include "XML\rapidxml_print.hpp" 
#include <cstdlib>
#include <ctime>

class XMLParser {


private:
	std::vector<LongMidFrame> longMidFrames;
	std::vector<CloseUpFrame> closeUpFrames;
	std::vector<OutOfFieldFrame> outOfFieldFrames;
	std::vector<FrameInfo> listOfFramesP;

	std::map<int, char*> emotionsMap;
public:
	XMLParser();
	~XMLParser();
	//Load XML File with the emotions data
	std::map<int,char*> loadEmotions();
	char* getEmotion(int index);
	//Save to XML File information extrated from every frame
	void saveFrameInfo();

	//Load from XML File information related with every frame
	void XMLParser::loadFrameInfo(std::string filename);

	std::vector<OutOfFieldFrame> getOutOfFieldFrames();
	std::vector<CloseUpFrame> getCloseUpFrames();
	std::vector<LongMidFrame> getLongMidFrames();
	std::vector<FrameInfo> getListOfFrames();

	void setOutOfFieldFrames(std::vector<OutOfFieldFrame> list);
	void setCloseUpFrames(std::vector<CloseUpFrame> list);
	void setLongMidFrames(std::vector<LongMidFrame> list);
	void setListOfFrames(std::vector<FrameInfo> list);


	/*
		UI GENERATE XML
	*/
	void saveQueryToXML(std::string type, std::vector<cv::Point> result);


	/*
		GENERATE VIDEO
	*/

	void saveGenerateVideoInfo(std::vector<int> list);
	std::vector<int> readGenerateVideoInfo(std::string filepath);

};
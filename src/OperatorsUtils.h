#include "opencv2\opencv.hpp"




	void drawLines(cv::Mat img);
	void erosion(int erosion_elem, int erosion_size, cv::Mat src, cv::Mat dst);
	void dilation(int dilation_elem, int dilation_size, cv::Mat src, cv::Mat dst);
	cv::Mat laplacianFilter(cv::Mat src);


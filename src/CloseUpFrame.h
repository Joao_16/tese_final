#pragma once
#include "FrameInfo.h"


class CloseUpFrame : public FrameInfo
{

private:
	std::vector<std::string> playerNames;
	std::vector<cv::Rect> playerFacesRect;
public: 
	CloseUpFrame(FrameInfo f);
	~CloseUpFrame();
	std::vector<std::string> getPlayerList();
	void setPlayerList(std::vector<std::string> players);
	std::vector<cv::Rect>  getPlayerFacesRect();
	void setPlayerFacesRect(std::vector<cv::Rect> faces);
};

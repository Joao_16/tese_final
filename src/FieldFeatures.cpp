#include "FieldFeatures.h"


using namespace cv;
using namespace std;


struct PointsLocation {
	int top;
	int bottom;
	int right;
	int left;
};


Mat detectField(Mat hsvimg) {
	int thresh = 20;

	Scalar minHSV = Scalar(35, 90, 90);
	Scalar maxHSV = Scalar(80, 255, 255);


	Mat maskHSV, resultHSV;
	inRange(hsvimg, minHSV, maxHSV, maskHSV);

	erosion(0, 1, maskHSV, maskHSV);
	

	bitwise_and(hsvimg, hsvimg, resultHSV, maskHSV);
	maskHSV.release();

	return resultHSV;
}


int fieldPercentage(Mat img) {

	Mat hsvChannels[3];
	split(img, hsvChannels);

	int nonzeroPix = countNonZero(hsvChannels[2]);
	hsvChannels->release();
	int totalPixels = img.cols*img.rows;

	return (nonzeroPix * 100 / totalPixels);

}

/*
	1) Analise a percentagem global
		Global % < 10 ---> Out of Field;
		Global % < 35 ---> Close-up;
	2) Diferenciar Close-up de Mid/Long Shot que possuam grande quantidade de relvado
	-> Analisar imagem por blocos com a divisao 3:5:3
		1) |0|1|2|-> percentagem media do bloco inferior a 30% trata-se de um Mid/Long Shot
		2) |3|4|5|-> analisar o meio da imagem pois normalmente e onde se encontram os jogadores em destaque
			Se uma das componentes for inferior a 50% trata-se de um jogador
		3) |0|    |2| Analisar estas duas componentes da imagem se a percentagem de verde for inferior a 50%
		   |3|    |5| considera-se que existe um jogador nao centrado na imagem
		   |6|    |8|
		4) Caso contrário trata-se de um Long/Mid Shot

	RESULT:
	0-Out Of Field
	1-Close Up
	2-Long Mid Shot

*/
int rankImage(int fieldP, vector<double> splitP) {

	if (fieldP <= 5) {
		return 0;
	}//35 antes
	else if (fieldP < 45){
		cout << "Close up";
		return 1;
	}
	else {
		double k = splitP.at(4); cout << k;
		//k < 65
		if (k < 60) {
			cout << "Close up";
			return 1;
		}
		else {

			double i = splitP.at(0) *(3.0 / 11.0) + splitP.at(1)*(5.0 / 11.0) + splitP.at(2)*(3.0 / 11.0);
			if (i < 30) {
				cout << "Long/Mid Shot";
				return 2;
			}
			else if (splitP.at(3) < 50 || splitP.at(4) < 50 || splitP.at(5) < 50) {

				cout << "Close-up shot";
				return 1;
			}
			else {
				i = splitP.at(0) *(3.0 / 11.0) + splitP.at(3)*(5.0 / 11.0) + splitP.at(6)*(3.0 / 11.0);
				double j = splitP.at(2) *(3.0 / 11.0) + splitP.at(5)*(5.0 / 11.0) + splitP.at(8)*(3.0 / 11.0);

				if (i < 50 || j < 50) {
					cout << "Close up";
					return 1;
				}
				else {
					cout << "Long/Mid Shot";
					return 2;
				}
			}
		}
	}
}


vector<Vec4i> detectFieldLines(Mat src) {


	Mat dst, cdst;
	Canny(src, dst, 50, 150, 3);

	cvtColor(dst, cdst, CV_GRAY2BGR);

		//METODO PROBABILISTICO DE HOUGH LINE DETECT
	vector<Vec4i> lines;
	HoughLinesP(dst, lines, 1, CV_PI / 180, 50, 10, 5);// default 50,50,10
	for (size_t i = 0; i < lines.size(); i++)
	{
		Vec4i l = lines[i];
		line(cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 1, CV_AA);
	}

	cout << "\nNumber of Lines: " << lines.size() << "\n";
	dst.release();
	
	return lines;
}

void rankLinesLocation(vector<Vec4i> lines, int width, int height)
{
	PointsLocation pointLoc = {0};
	
	for (unsigned int i = 0; i < lines.size(); i++) {
		Vec4i line = lines[i];
		Point a = Point(line[0], line[1]);
		Point b = Point(line[2], line[3]);
		processPoint(pointLoc, a, width, height);
		processPoint(pointLoc, b, width, height);
		}


	cout << "\nTOP:" << pointLoc.top << "\n";
	cout << "BOTTOM:" << pointLoc.bottom << "\n";
	cout << "LEFT:" << pointLoc.left << "\n";
	cout << "RIGHT:" << pointLoc.right << "\n";
}


void processPoint(PointsLocation &pointLoc ,Point x, int width, int height) {
	if (x.x < width / 2)
		pointLoc.left++;
	else
		pointLoc.right++;
	if (x.y < height / 2)
		pointLoc.top++;
	else
		pointLoc.bottom++;
}




/*
	//////////////////////////////////////////////////////////////////////////
					Rever e melhorar estes algoritmos
	//////////////////////////////////////////////////////////////////////////
*/

void detectFieldCircles(cv::Mat src) {

	Mat src_gray,dst;

	/// Reduce the noise so we avoid false circle detection
	GaussianBlur(src, dst, Size(9, 9), 2, 2);
	medianBlur(dst, dst, 3);

	/// Convert it to gray
	cvtColor(dst, src_gray, CV_BGR2GRAY);

	vector<Vec3f> circles;

	/// Apply the Hough Transform to find the circles
	HoughCircles(src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows / 8, 30, 15, 0, 0);

	/// Draw the circles detected
	for (size_t i = 0; i < circles.size(); i++)
	{
		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		int radius = cvRound(circles[i][2]);
		// circle center
		circle(src, center, 3, Scalar(0, 255, 0), -1, 8, 0);
		// circle outline
		circle(src, center, radius, Scalar(0, 0, 255), 3, 8, 0);
	}

	/// Show your results
	imshow("Hough Circle Transform Demo", src);

}



void locateFrame(vector<Vec4i> lines)
{
	
	for (unsigned int i = 0; i < lines.size(); i++) {
		if (!checkCloseLines(lines, i))
			lines.erase(lines.begin() + i);
	}
	cout << "Number of Lines: " << lines.size();

}

bool checkCloseLines(vector<Vec4i> lines, int pos) {

	int nLines = lines.size();
	Vec4i point = lines.at(pos);
	Point a = Point(point[0], point[1]);
	Point b = Point(point[2], point[3]);

	for (int i = 0; i < nLines; i++) {
		if (i != pos) {
			Vec4i l = lines[i];
			Point aux1 = Point(l[0], l[1]);
			Point aux2 = Point(l[2], l[3]);
			if (norm(a - aux1) < 10)
				if (abs(b.x - aux2.x) < 20 || abs(b.y - aux2.y) < 20)
					return false;
			else if	(norm(a - aux2) < 20)
				if (abs(b.x - aux1.x) < 20 || abs(b.y - aux1.y) < 20)
					return false;
			else if (norm(b - aux1) < 20)
				if (abs(a.x - aux2.x) < 20 || abs(a.y - aux2.y) < 20)
					return false;
			else if (norm(b - aux2) < 10)
				if (abs(a.x - aux1.x) < 20 || abs(a.y - aux1.y) < 20)
					return false;
		}
	}
	return true;
}


/*

			Automatic Dominant Color Extraction

*/


Point mainColorThreshold(Mat img) {

	MatND hist = getHistogramHSV2(img);

	double min, max;
	Point min_loc, max_loc;
	minMaxLoc(hist, &min, &max, &min_loc, &max_loc);
	
	int base = 180 / 16;
	int minH = base * max_loc.y;
	int maxH = minH;

	int threshold = 45;

	minH += 5;
	maxH += threshold;

	Point hTthrehold = Point(minH, maxH);

	//cout << "min: "<< hTthrehold.x <<" max: "<< hTthrehold.y << "\n";

	return hTthrehold;
}


MatND getHistogramHSV2(Mat img) {

	Mat hsv;
	cvtColor(img, hsv, COLOR_BGR2HSV);

	// Quantize the hue to 30 levels
	// and the saturation to 32 levels
	int hbins = 16, sbins = 32;
	int histSize[] = { hbins, sbins };
	// hue varies from 0 to 179, see cvtColor
	float hranges[] = { 0, 180 };
	// saturation varies from 0 (black-gray-white) to
	// 255 (pure spectrum color)
	float sranges[] = { 0, 256 };
	const float* ranges[] = { hranges, sranges };
	MatND hist;
	// we compute the histogram from the 0-th and 1-st channels
	int channels[] = { 0, 1 };

	calcHist(&hsv, 1, channels, Mat(), // do not use mask
		hist, 2, histSize, ranges,
		true,
		false);


	return hist;
}

Mat detectFieldWithDynamicThreshold(Mat hsvimg,Point threshold) {

	Scalar minHSV = Scalar(threshold.x, 50, 50);
	Scalar maxHSV = Scalar(threshold.y, 255, 255);

	Mat maskHSV, resultHSV;
	inRange(hsvimg, minHSV, maxHSV, maskHSV);

	erosion(0, 1, maskHSV, maskHSV);

	bitwise_and(hsvimg, hsvimg, resultHSV, maskHSV);

	maskHSV.release();
	return resultHSV;
}
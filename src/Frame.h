#include "opencv2\opencv.hpp"
#include "FieldFeatures.h"

class Frame
{

private:
	std::vector<cv::Mat> splitFrame;
	std::vector<double> splitFrameP;
	cv::Mat maskImg;

public:
	Frame(std::vector<cv::Mat> splitF, cv::Mat mask);
	~Frame();
	std::vector<cv::Mat> getSplitFrame();
	void setSplitFrame(std::vector<cv::Mat> vec);
	void setMask(cv::Mat mask);
	cv::Mat getMaskImg();
	cv::Mat Frame::getSplitFrameAt(int i);
	void calculateFieldPercentage();
	std::vector<double> getPercentage();
};

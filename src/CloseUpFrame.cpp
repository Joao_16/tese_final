#include "CloseUpFrame.h"

CloseUpFrame::CloseUpFrame(FrameInfo f)
{
	setID(f.getID());
	setGameResult(f.getGameResult());
	setGameTime(f.getGameTime());
	setType(f.getType());
	setVideoTime(f.getVideoTime());
}

CloseUpFrame::~CloseUpFrame()
{
}

std::vector<std::string> CloseUpFrame::getPlayerList()
{
	return playerNames;
}

void CloseUpFrame::setPlayerList(std::vector<std::string> players)
{
	this->playerNames = players;
}

std::vector<cv::Rect> CloseUpFrame::getPlayerFacesRect()
{
	return playerFacesRect;
}

void CloseUpFrame::setPlayerFacesRect(std::vector<cv::Rect> faces)
{
	playerFacesRect = faces;
}

#include "FeatureExtraction.h"

using namespace cv;
using namespace std;

FeatureExtraction::FeatureExtraction()
{
}

FeatureExtraction::FeatureExtraction(cv::Mat img)
{
	this->originalImg = img;
}

FeatureExtraction::~FeatureExtraction()
{
}

cv::Mat FeatureExtraction::getOriginalImg()
{
	return this->originalImg;
}

std::vector<std::string> FeatureExtraction::getText()
{
	return text;
}

long FeatureExtraction::getVideoTime()
{
	return 0;
}


int FeatureExtraction::getFrameType(cv::Mat img)
{
	Mat hsvimg, resultHSV;
	cvtColor(img, hsvimg, COLOR_BGR2HSV);

	resultHSV = detectField(hsvimg);
	Frame *f = new Frame(splitFrame(resultHSV), resultHSV);

	int i = fieldPercentage(resultHSV);

	int result = rankImage(i, f->getPercentage());
	f->~Frame();
	intersectWithMask(img, resultHSV);
	hsvFrame = resultHSV;
	resultHSV.release(); img.release();
	return result;
}

int FeatureExtraction::getFrameType(cv::Mat img, cv::Point colorThreshold)
{
	Mat hsvimg, resultHSV;
	cvtColor(img, hsvimg, COLOR_BGR2HSV);

	resultHSV = detectFieldWithDynamicThreshold(hsvimg,colorThreshold);
	Frame *f = new Frame(splitFrame(resultHSV), resultHSV);

	int i = fieldPercentage(resultHSV);

	int result = rankImage(i, f->getPercentage());
	f->~Frame();
	intersectWithMask(img, resultHSV);
	hsvFrame = resultHSV;

	resultHSV.release(); img.release();
	return result;
}




int FeatureExtraction::getFieldLocation(cv::Mat img)
{
	setupBOWDescriptor();
	int result = evaluateImage(img);
	return result;
}

//EXECUTE NUMBEROFPLAYERS BEFORE TEAMNAMES
std::vector<int> FeatureExtraction::getNumberOfPlayers(cv::Mat img)
{
	setPlayersTeam(extractPlayers(originalImg, findPlayerContours(hsvFrame)));
	
	return getTotalPlayers();
}

std::vector<std::string> FeatureExtraction::getTeamNames(cv::Mat img)
{
	vector<string> teams;
	vector <char*>teamsChar = getTeams();
	for (int i = 0; i < teamsChar.size(); i++)
		teams.push_back(string(teamsChar.at(i)));

	return teams;
}

std::vector<std::string> FeatureExtraction::getPlayerNames(cv::Mat img)
{
	vector<Mat> facesMat = detectPlayersFaces(originalImg);
	vector<string> names = identifyPlayerDetected(facesMat);

	return identifyPlayerDetected(facesMat);
}

std::vector<cv::Rect> FeatureExtraction::getFacesRect()
{
	return getPlayerFacesRect();
}


//EXECUTE GAME TIME BEFORE GAME RESULT
//Check time result first before calling getCurrentGameResult

std::string FeatureExtraction::getCurrentGameTime(cv::Mat img)
{
	
	string time = "";
	text = processScoreBorder(img);
	if (text.size() > 2) {
		time = getTime(text);
		if (time.compare("") == 0 || time.length() < 5)
			return "";
	}
	time.erase(std::remove(time.begin(), time.end(), '\n'), time.end());
	//char *cstr = new char[time.length() + 1];
	//strcpy(cstr, time.c_str());
	//return cstr;
	return time;
}

std::string FeatureExtraction::getCurrentGameResult()
{
	return getResult(text);
}
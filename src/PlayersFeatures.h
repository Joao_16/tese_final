#include "opencv2\opencv.hpp"
#include "OperatorsUtils.h"
#include <string>
#include <fstream> 




/*
-----------------------------------------------------------------------------------------------------
								Detect players Mid/Long Shot
-----------------------------------------------------------------------------------------------------
*/


std::vector<std::vector<cv::Point> > findPlayerContours(cv::Mat img);

std::vector<std::vector<cv::Point> > findRectIntersect(std::vector<std::vector<cv::Point> > contours);

/*
	Retira da imagem original os rectangulos onde os jogadores foram detectados
*/
std::vector<cv::Mat> extractPlayers(cv::Mat img, std::vector<std::vector<cv::Point> > contours);

std::vector<cv::Rect> processMerge(std::vector<cv::Rect> faces, std::vector<cv::Rect> profile);


void setPlayersTeam(std::vector <cv::Mat> players);

bool less_vectors(const std::vector<cv::Mat> a, const std::vector<cv::Mat> b);

cv::MatND getHistogramRGB(cv::Mat player);

cv::MatND getHistogramHSV(cv::Mat player);

cv::Mat histogramEqualization(cv::Mat player);

double compareHistogram(cv::MatND hist1, cv::MatND hist2);

std::vector <char*> attributeTeam(std::vector<std::vector<cv::MatND>> players);

std::string processTeamName(std::string teamName);

std::vector<int> getTotalPlayers();
std::vector <char*> getTeams();

void setPlayerTeamTemplatePath(std::string path);






/*
-----------------------------------------------------------------------------------------------------
								Detect players Close-up Shot
-----------------------------------------------------------------------------------------------------
*/


/*
Detectar nos frames a localizacao dos v�rios jogadores e arbitro
*/
std::vector<cv::Mat> detectPlayersFaces(cv::Mat img);



/*
Identificar a identidade do jogador detectado, para os frames close-up
*/

std::vector<std::string> identifyPlayerDetected(std::vector<cv::Mat> playerFaces);

void loadFaceDB();

void loadPlayerNames();

 std::string processFilename(std::string playername);

 void setPlayerFacesPath(std::string path);

 std::vector<cv::Rect> getPlayerFacesRect();
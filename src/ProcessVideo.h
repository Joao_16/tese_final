#include "FeatureExtraction.h"



class ProcessVideo {

private:
	FeatureExtraction extractor;
	std::string videoPath;
	cv::VideoCapture cap;
	int totalFrames;
	int currentID;
	long currentVideoTime;
	cv::Point colorThreshold;
	bool colorThresholdExtracted;

	std::vector<FrameInfo*> listOfFrames;
	std::vector<LongMidFrame> listOfLongMid;
	std::vector<CloseUpFrame> listOfCloseUp;
	std::vector<OutOfFieldFrame> listOfOutOfField;

public:
	ProcessVideo();
	ProcessVideo(std::string video_Path);
	~ProcessVideo();
	cv::Mat setNextFrame();
	cv::Mat getFrameAtIndex(double index);
	void processAllFrames();
	void processFrame(cv::Mat img);
	cv::Mat getScoreBorder(cv::Mat img, int x, int y, int w, int h);
	int  ProcessVideo::getCurrentVideoSecond(int frameID);
	int  ProcessVideo::getTotalFrames();

	FrameInfo createFrameInfo(cv::Mat img);
	LongMidFrame createLongMidFrame(FrameInfo f,cv::Mat img);
	CloseUpFrame createCloseUpFrame(FrameInfo f, cv::Mat img);
	OutOfFieldFrame createOutOfFieldFrame(FrameInfo f);

	void printFrameInfo(int index);

	/*
		GENERATE HIGHLIGHT VIDEO
	*/

	void generateVideo(std::string videoPath);
	std::vector<int> getFrameTypes(cv::VideoCapture video);

	void splitVideo(cv::VideoCapture video, std::vector<int> dataIndex);

};
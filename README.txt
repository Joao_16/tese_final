FRAMEWORKS/PROGRAMAS USADOS:

-Visual Studio IDE, em modo Release 32bits, por causa da biblioteca OCR, que s� funciona neste modo.

-OpenCV 2.4.13 - pois possu� v�rias bibliotecas que foram separadas na vers�o 3.0 para a frente;

-OCR Tesseract - informa��o de instala��o presente no link: 
https://stackoverflow.com/questions/18180824/how-to-implement-tesseract-to-run-with-project-in-visual-studio-2010
Primeira resposta;

- QT - plugin no Visual Studio, vers�o 5.10;

PRINCIPAIS PASSOS DE INSTALA��O:

- Configurar OpenCV no VS;
- Configurar OCR Tesseract no VS e testar com exemplo simples;
- Configurar plugin QT, para as componentes visuais do projecto;

Organiza��o das classes pode ser consultada no relat�rio da tese;

-Na classe main existe algum c�digo comentado que representa ambientes de teste dos v�rios componentes;

-O principal v�deo de teste corresponde ao per�odo de prolongamento do jogo entre Portugal e Fran�a no Euro 2016.

#pragma once

#include <QtWidgets/QMainWindow>
#include <qgraphicsview.h>
#include "ui_MainWindow.h"
#include "src\UIDataConnect.h"
#include <string>
#include <QFileDialog>
#include <qmessagebox.h>
#include <qpixmap.h>
#include <qcheckbox.h>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);
	void MainWindow::setupImageView(cv::Mat img);
	void MainWindow::setupMenuButtonClicked();
	void MainWindow::setupFrameInfoButtons();
	void MainWindow::setupFrameInfoLabels();
	void MainWindow::setupGenerateUploadButtons();
	void MainWindow::setupLocationMap();
	void MainWindow::setupLocationButtons();
	void MainWindow::drawLocationPoints(int part);
	void MainWindow::setupQueryPickers();
	void MainWindow::setupQueryButtonClicked();
	void MainWindow::setupQuerryLabel();
	void setupImageQueryView(cv::Mat img);
	void MainWindow::setupPlayerNamesView();

	//generate video
	void MainWindow::setupHighlightButtons();

private:
	Ui::MainWindowClass ui;
	ProcessVideo pVideo;
	UIDataConnect data;
	int currentIndex;
	std::vector<QLabel*> labels;
	int currentPart;
	std::vector<cv::Point> queryResult;
	int currentQuerryIndex;
	std::vector<QCheckBox*> checkBoxes;
	std::string queryType;
	int selected_loc;
	QPushButton *field;

	std::string teamPath;
	std::string rightTeam;
	std::string leftTeam;

public slots:
	void displayWindow();
	void frameInfoButtonControl();
	void upLoadBox();
	void generateBox();
	void on_frame_type_picker_toggled(bool checked);
	void on_location_picker_toggled(bool checked);
	void on_players_picker_toggled(bool checked);
	void on_radioButton_2_toggled(bool checked);
	void generateSearchQueryBox();
	void querryButtonHanddle();
	void partSelected();
	void fieldButtonClicked();
	void teamSideHanddler();
	void checkMenu(QObject* button);
	void generateHighlightVideo();

};
